//
//  Refund.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-29.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct refundData {
    static let generalTerms = """
    In case you have technical issues with the Kidsmana application, which cannot be fixed by Kidsmana Customer Support Center, you may be eligible for a full refund in accordance with Refund Policy outlined below. However, we are convinced that most of claims for refund may be avoided if you accept professional help from our Customer Support Center. To contact the Customer Support Center please write to onlinesupport@kidsmana.com .
"""
    static let refundConditions = """
    Subject to this Refund Policy, you may be eligible to receive a full refund within 2 hours following the time of your purchase provided your reasons don’t contradict with the Refund Policy conditions outlined below.

    No refunds can be issued to a user for the following reasons:

    * If 2 hours have passed since the time of purchase.
    * If a user refuses to re-install Kidsmana application if the operating system is updated on the target phone.
    * If the user's target device does not comply with the Kidsmana Compatibility Policy.
    Only the following operating system is supported: Android. Compatible versions for this system are: Android >= 4.1 Some features of the Kidsmana application require a specific Android version, these features will not work if your device is not compatible, so no refunds will be issued if these features do not work for you.
    * If the target phone has lost the connection with the Kidsmana subscription due to lack of internet access, factory reset or update to the last version of the operating system.
    * If the target phone is not the property of a user or if a user does not receive the consent of a target phone owner to install the Kidsmana application or if the user has forgotten the word pass to unlock it.
    * If the target phone does not have Internet access (no money left on the target phone account, temporary service interruption, roaming-related problem, etc.).
    * If the target phone was reset to original factory settings.
    * If a user does not follow the setup guidelines stated on our site or from our customer support team or does not accept technical assistance.
    * If a user did not receive the data that was saved on a target phone before Kidsmana was installed on it.
    * If the Kidsmana application has been disconnected or damaged by an anti-virus application, the owner of a target device, or other services.
    * For personal reasons (I changed my mind, I made a purchase by mistake, the application was not used, etc.).
    Your refund may be issued only once. If you buy another Kidsmana subscription at a later time, it cannot be submitted to the same request for reimbursement.
    """
    
    static let refundProcedure = """
    You must send your refund request at onlinesupport@kidsmana.com . We do not accept refund requests made in any other way. The refund decision must be made within 2 hours since the time of purchase.
    """
    static let chargeBackRelated = """
    In the event of a chargeback by a credit card company (or similar action by another payment provider allowed by us) in connection with your purchase of any subscription plan(s), you agree that we may suspend access to any and all accounts you have with us. Account’s reactivation is processed at our discretion and only after our receipt of the unpaid purchase(s).Charges for the service(s) which use our credit card payment processor will be identified on your credit card statement. Fees, incurred as the result of chargebacks or other payment disputes brought by you, your bank, or a Payment Provider, and disputes that require accounting or legal services shall be covered by you.
    """
    static let miniDetail = """
This policy governs your purchase of Kidsmana App subscription. Please be sure to read this carefully before completing your order as it sets out your rights with respect to your purchases, including essential restrictions and exclusions.
"""

}

struct privacyPolicy {
    static let introduction = """
    We manage our business with the principles indicated in this Privacy Policy in order to make sure that the confidentiality of personal information is secured and managed. Users’ access to the Mobile Application and use of the Services offered on the Kidsmana application is subject to this Privacy Policy. By accessing the Mobile Application and by sustaining to use the Services provided, you accept this Privacy Policy, and in particular, you are deemed to have consented to our use and disclosure of your personal information in the manner described hereby. We reserve the right to make changes to this Privacy Policy at any time. If you do not agree with any part of this Privacy Policy, you must immediately discontinue accessing the Mobile Application and using the Services.
    """
    static let infoWeProcess = """
    As part of the regular functionality of our Services, we collect, use information about you. This Privacy Policy has been developed in a way for you to understand how we collect, use, communicate and disclose and make use of your personal information when the Services on the Mobile Application are used

    We will not sell, use, or disclose any data for any purpose with third parties.

    We will describe the reasons for which information is being collected, before or at the time of collecting personal information from you:

    Registration and Profile Information. When you create an account, we may collect your personal information such as your username, first and last name, email address, mobile phone number and photo, if you chose to have a photo associated with your account. We may collect billing information when you sign up for Premium service. The data we receive is dependent upon your privacy settings with the social network.

    Geolocation. We collect your unique user identifier and your location through GPS, WiFi, or wireless network triangulation in order to obtain your location for the purposes of providing our Service. We collect the location of your designated Location Alerts to provide notifications when arrive and leave special Place. If you turn off location sharing we will not share your location with your family members; however, while you are logged in to your account, we will continue to collect your location information in order to provide the Service (for example, to provide our services or to share your location with members you allow to see your location). We maintain location information only so long as is reasonable to provide the Service and then destroy the location data.
    """
    static let purpose = """
    To make our service available. We use information that you submit and information that is processed automatically to provide you with all requested services, such as: create your account and identify you as a user.

    To improve, test and monitor the effectiveness of our App. We use information that is processed automatically to better understand user behavior and trends, detect potential outages and technical issues, to operate, protect, improve, and optimize our App.
    """
    static let long = """
    We will only keep personal information as long as needed to fulfill the purposes identified. We will protect personal information by reasonable security protections against loss or theft, unauthorized access, disclosure, copying, or modification.
    """
    static let chnages = """
    Because we're always looking for new and innovative ways to help you enjoy our services, this policy may change over time. We will notify you of any material changes so that you have time to review the changes.
    """
    static let eu = """
    Depending on your Privacy Settings, our partners may collect and process personal data such as device identifiers, language, location data, geolocation and other demographic and interest data about you to provide you with the most relevant experience when using this app.
    """
    static let miniDetail = """
    Please read this Privacy Policy carefully before using the app
    """
}

struct tos {
    static let tou = """
    By downloading, accessing, browsing or using this mobile application (“Kidsmana”), Get Location, you agree to be obligated by these Terms and Conditions of Use. We reserve the right to change these Terms and Conditions at any time. If you disagree with any of these Terms and Conditions of Use, you must immediately stop your access to the Mobile Application and your use of the services offered by the Mobile Application. Continued use of the Mobile Application will constitute the acceptance of these Terms and Conditions of Use, which may be changed from time to time
    """
    static let def = """
    In these Terms and Conditions of Use, the following terms shall have the following definitions, except where the context otherwise requires: “We” is used to refer to Kidsmana. "Users" means users of the Mobile Application, including you and "User" means any one of them. "Account" means an account created by a User on the Mobile Application as part of Registration. "Merchant" refers to any entity whose products or services can be purchased via the Mobile Application. "Privacy Policy" means the Privacy Policy set out for this Mobile Application. "Register" means to create an Account on the Mobile Application and "Registration" means the act of creating such an Account. "Service/Services" means all the services provided by Kidsmana. via the Mobile Application to Users
    """
    static let info = """
    1.The use of any Services and/or the Mobile Application is subject to these Terms and Conditions of Use.
    2. The Mobile Application and the Services are for your non-commercial, personal use only and they must not be used for business purposes.
    3. The provision of the Services and the Mobile Application does not include the provision of a mobile telephone or handheld device or other necessary equipment to access the Mobile Application or the Services. To use the Mobile Application or Services, you will require Internet connections. You acknowledge that the terms of agreement with your own mobile network provider will continue to apply when using the Mobile Application, Get Location. As a result, you will be charged by your Mobile Provider to access the internet while using the Mobile Application or any such third party charges may arise. You must take responsibility for any such charges that will arise.
    4. If you are not the payer for the invoice of the mobile telephone or handheld device that you are using to access the Mobile Application, Kidsmana will to have assume that permission from the invoice payer for using the Mobile Application has been obtained. 5. By sending any text, images or photographs (“Material”) via the Mobile Application, you claim that you are the owner of the Material, or have the required permission from the owner of the Material to use, reproduce and distribute it. You hereby give us a worldwide, royalty-free, non-exclusive license to use the Material to promote any products or services
    """
    static let notifi = """
    You accept to receive pre-defined notifications (“Location Alerts”) on the Mobile Application from Merchants if you have turned on related services on your mobile telephone or other handheld device
    """
    static let rules = """
    1. Depending on your Privacy Settings, we may collect and process personal data such as device identifiers, language, location data, geolocation and other demographic and interest data about you to provide you with the most relevant experience when using this app.Follo Sdn. Bhd. will make the necessary efforts to correct any errors or omissions as soon as being notified of them. However, we do not guarantee that the Services or the Mobile Application will have no faults, and we do not accept liability for any such faults, errors or omissions. In the event of any such error, fault or omission, “Users” should report it by contacting us.
    2. We do not warrant that your use of the Services or the Mobile Application will be uninterrupted and we do not warrant that any information (or messages) transmitted via the Services or the Mobile Application will be transmitted accurately, reliably, in a timely manner or at all. Notwithstanding that we will try to allow uninterrupted access to the Services and the Mobile Application, access to the Services and the Mobile Application may be suspended, restricted or terminated at any time.
    3. We do not give any warranty that the Services and the Mobile Application are free from viruses or anything else that may have a harmful effect on any technology.
    4. We reserve the right to change, modify, substitute, suspend or remove without notice any information or Services on the Mobile Application from time to time. Your access to the Mobile Application and/or the Services may also be occasionally restricted to allow for repairs, maintenance or the introduction of new facilities or services. We will attempt to restore such access as soon as we reasonably can. For the avoidance of doubt, we reserve the right to withdraw any information or Services from the Mobile Application at any time.
    5. We reserve the right to block access to and/or to edit or remove any material, which in our reasonable opinion may violate these Terms and Conditions of Use
    """
    static let dis = """
    1. The Mobile Application, the Services, the information on the Mobile Application and use of all related services are provided on an "as is, as available" basis without any warranties whether expressed or implied.
    2. To the limits permitted by applicable law, we disclaim all representations and warranties relating to the Mobile Application and its contents, including any inaccuracies or omissions in the Mobile Application, warranties of merchantability, quality, accuracy, availability, non-infringement or implied warranties from course of dealing or usage of trade.
    3. We do not warrant that the Mobile Application will always be accessible, uninterrupted, timely, secure, error free or free from viruses or other invasive or damaging code or that the Mobile Application will not be affected by any force majeure events, including inability to obtain or shortage of necessary materials, equipment facilities, power or telecommunications, lack of telecommunications equipment or facilities and failure of information technology or telecommunications equipment or facilities.
    4. Although we shall make maximum efforts to include accurate and up-to- date information on the Mobile Application, we give no guarantee or representations as to its accuracy, timeliness or completeness.
    5. We cannot be held liable for any acts or omissions of any third parties resulting from or in connection with the mobile application and the services offered in the mobile application, your access to, use of or inability to use the mobile application or the services offered in the mobile application, reliance on or downloading from the mobile application and/or services, or any delays, inaccuracies in the information or in its transmission including but not limited to damages for loss of business or profits, use, data or other intangible, even if we have been advised of the possibility of such damages.
    6. We shall not be liable for any indirect, consequential, collateral, special or incidental loss or damage suffered or incurred by you in connection with the Mobile Application and these Terms and Conditions of Use. For the purposes of these Terms and Conditions of Use, indirect or consequential loss or damage includes, without limitation, loss of revenue, profits, anticipated savings or business, loss of data or goodwill, loss of use or value of any equipment including software, claims of third parties, and all associated and incidental costs and expenses.
    7. The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer that cannot be excluded or limited are affected.
    8. Notwithstanding our efforts to ensure that our system is secure, you acknowledge that all electronic data transfers are potentially susceptible to interception by others. We cannot, and do not, warrant that data transfers pursuant to the Mobile Application, or electronic mail transmitted to and from us, will not be monitored or read by otherS
    """
    static let inde = """
    1. All data, content, information, photographs, illustrations, other graphic materials, and names, logos and trade marks on the Mobile Application are protected by copyright laws and/or other laws and/or international treaties, and belong to us and/or our suppliers, as the case may be. These works, logos, graphics, sounds or images may not be copied, reproduced, retransmitted, distributed, disseminated, sold, published, broadcasted or circulated whether in whole or in part, unless expressly permitted by us and/or our suppliers, as the case may be.
    2. Misuse of any trademarks or any other content displayed on the Mobile Application is prohibited.
    3. We will take legal action against any unauthorized usage of our trademarks, name or symbols to preserve and protect its rights. All rights not expressly granted herein are reserved. Other product and company names mentioned herein may also be the trademarks of their respective owners
    """
    static let chnages = """
    1. Follo Sdn. Bhd. may make amendments to the contents of the Mobile Application, including to the descriptions and prices of goods and services advertised, at any time and without notice. We assume no liability or responsibility for any errors or omissions in the content of the Mobile Application.
    2. We reserve the right to change these Terms and Conditions of Use from time to time without notice. The updated Terms and Conditions of Use will be posted on the Mobile Application and shall take effect from the date of such posting. We advise you to check these terms and conditions periodically as they are binding upon you
    """
    static let sub = """
    1.If you use (or anyone other than you, with your permission uses) the Mobile Application, any Services in breach of these Terms and Conditions of Use, we may suspend your use of the Services and/or Mobile Application.
    2. If we suspend the Services or Mobile Application, we may refuse to restore the Services or Mobile Application for your use until we receive an assurance from you, in a form we deem acceptable, that there will be no further infringement of the items of these Terms and Conditions of Use.
    3. Follo Sdn. Bhd. shall fully co-operate with any law enforcement authorities or court order requesting or directing Kidsmana. to disclose the identity or locate anyone in violation of these Terms and Conditions of Use.
    4. Without limitation to anything else in this, we shall be entitled immediately or at any time (in whole or in part) to:
    a. suspend the Services and/or Mobile Application;
    b. suspend your use of the Services and/or Mobile Application; and/or
    c. suspend the use of the Services and/or Mobile Application for people we believe to be connected to you, if:
    i. you commit any violation of these Terms and Conditions of Use;
    ii. we suspect, on reasonable grounds, that you have, might or will violate these Terms and Conditions of Use; or
    iii. We suspect, on reasonable grounds, that you may have committed or be committing any fraud against us or any person.
    iv. Our rights under this Clause shall not prejudice any other right or remedy we may have in respect of any breach or any rights, obligations or liabilities accrued prior to termination.
    """
    static let susp = """
1.If you use (or anyone other than you, with your permission uses) the Mobile Application, any Services in breach of these Terms and Conditions of Use, we may suspend your use of the Services and/or Mobile Application.
2. If we suspend the Services or Mobile Application, we may refuse to restore the Services or Mobile Application for your use until we receive an assurance from you, in a form we deem acceptable, that there will be no further infringement of the items of these Terms and Conditions of Use.
3. Follo Sdn. Bhd. shall fully co-operate with any law enforcement authorities or court order requesting or directing Kidsmana. to disclose the identity or locate anyone in violation of these Terms and Conditions of Use.
4. Without limitation to anything else in this, we shall be entitled immediately or at any time (in whole or in part) to:
a. suspend the Services and/or Mobile Application;
b. suspend your use of the Services and/or Mobile Application; and/or
c. suspend the use of the Services and/or Mobile Application for people we believe to be connected to you, if:
i. you commit any violation of these Terms and Conditions of Use;
ii. we suspect, on reasonable grounds, that you have, might or will violate these Terms and Conditions of Use; or
iii. We suspect, on reasonable grounds, that you may have committed or be committing any fraud against us or any person.
iv. Our rights under this Clause shall not prejudice any other right or remedy we may have in respect of any breach or any rights, obligations or liabilities accrued prior to termination.
"""
    static let law = """
    The Mobile Application can be accessed from all countries around the world as long as the local technology permits. As all countries have different laws, by accessing the Mobile Application both you and we agree that the laws of the country, without regard to the conflicts of laws principles thereof, will apply to all issues relating to the use of the Mobile Application. You accept and agree that both you and we shall submit to the exclusive jurisdiction of the courts of the country in respect of any dispute arising out of and/or in connection with these Terms and Conditions of Use.
    """
    static let mini = """
    Please read this Shipping/Fulfillment Policy
    """
}
