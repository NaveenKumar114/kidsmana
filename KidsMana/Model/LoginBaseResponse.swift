//
//  LoginBaseResponse.swift
//  epod
//
//  Created by Karthikeyan R on 26/11/19.
//  Copyright © 2019 Karthikeyan R. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginBaseResponse = try? newJSONDecoder().decode(LoginBaseResponse.self, from: jsonData)

import Foundation

// MARK: - LoginBaseResponse
struct LoginBase: Codable {
    let code: Int?
    let response: String?
    let client: Client?
}

// MARK: - Client
struct Client: Codable {
    let clientIDPrimary, userID, createdBy, addedDate: String?
    let clientName, clientCode, isActive, createdOn: String?
    let updatedOn, updatedBy, branchID, clientID: String?
    let branchCode, branchContactName, branchContactPhone, mainBranchid: String?
    let address, zipCode, clientEmail, clientFax: String?
    let phoneNumber, locationAddress, latitude, longitude: String?
    let username, password, tokenid: String?
    
    enum CodingKeys: String, CodingKey {
        case clientIDPrimary = "client_id_primary"
        case userID = "user_id"
        case createdBy
        case addedDate = "added_date"
        case clientName = "client_name"
        case clientCode = "client_code"
        case isActive = "is_active"
        case createdOn = "created_on"
        case updatedOn = "updated_on"
        case updatedBy
        case branchID = "branch_id"
        case clientID = "client_id"
        case branchCode = "branch_code"
        case branchContactName = "branch_contact_name"
        case branchContactPhone = "branch_contact_phone"
        case mainBranchid = "main_branchid"
        case address
        case zipCode = "zip_code"
        case clientEmail = "client_email"
        case clientFax = "client_fax"
        case phoneNumber = "phone_number"
        case locationAddress = "location_address"
        case latitude, longitude, username, password, tokenid
    }
}
