//
//  AllLocation.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-05.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct AllLocation: Codable {
    let code: Int?
    let response: String?
    let employeelist: [Employeelist]
}

// MARK: - Employeelist
struct Employeelist: Codable {
    let id, deviceID, name, email: String?
    let icno, mobile, countryCode, designation: String?
    let department, issueDate, gender, password: String?
    let lastLogin, status, role, bannedUsers: String?
    let subscriberID, profile, createdBy, createdDate: String?
    let modifyBy, modifyDate, latitude, longitude: String?
    let profileImage, speed, distance, appiconHideshow: String?
    let tokenid, mobileTime, dailyTimeLimit, mobileTimeEnable: String?
    let deviceAdminEnable: String?

    enum CodingKeys: String, CodingKey {
        case id
        case deviceID = "device_id"
        case name, email, icno, mobile
        case countryCode = "country_code"
        case designation, department
        case issueDate = "issue_date"
        case gender, password
        case lastLogin = "last_login"
        case status, role
        case bannedUsers = "banned_users"
        case subscriberID = "subscriber_id"
        case profile
        case createdBy = "created_by"
        case createdDate = "created_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
        case latitude, longitude
        case profileImage = "profile_image"
        case speed, distance
        case appiconHideshow = "appicon_hideshow"
        case tokenid
        case mobileTime = "mobile_time"
        case dailyTimeLimit = "daily_time_limit"
        case mobileTimeEnable = "mobile_time_enable"
        case deviceAdminEnable = "device_admin_enable"
    }
}
