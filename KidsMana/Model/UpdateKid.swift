//
//  UpdateKid.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-10.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation
struct UpdateKid: Codable {
    let code: Int?
    let response: String?
    let kid: UKid?
}

// MARK: - Kid
struct UKid: Codable {
    let subscriberID, name, role, modifyBy: String?
    let profile, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case subscriberID = "subscriber_id"
        case name, role
        case modifyBy = "modify_by"
        case profile
        case modifyDate = "modify_date"
    }
}
