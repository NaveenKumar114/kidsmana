//
//  AppDetails.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-02.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

// MARK: - AppDetails
struct AppDetails: Codable {
    let code: Int
    let response: String
    let appusagelist: [Appusagelist]
}

// MARK: - Appusagelist
struct Appusagelist: Codable {
    let appid, name, pkgName, createdate: String
    let createby, modifyby, modifydate, appiconHideshow: String
    var appTime, appTimeLimit, appTimeLimitEnable, rootclass: String
    let mainclass, eventTime, eventType, mobile: String
    let count, usageTime, createdateAu, createbyAu: String
    let modifybyAu, modifydateAu: String

    enum CodingKeys: String, CodingKey {
        case appid, name
        case pkgName = "pkg_name"
        case createdate, createby, modifyby, modifydate
        case appiconHideshow = "appicon_hideshow"
        case appTime = "app_time"
        case appTimeLimit = "app_time_limit"
        case appTimeLimitEnable = "app_time_limit_enable"
        case rootclass, mainclass
        case eventTime = "event_time"
        case eventType = "event_type"
        case mobile, count
        case usageTime = "usage_time"
        case createdateAu = "createdate_au"
        case createbyAu = "createby_au"
        case modifybyAu = "modifyby_au"
        case modifydateAu = "modifydate_au"
    }
}
