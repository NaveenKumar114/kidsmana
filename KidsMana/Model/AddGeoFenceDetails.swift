//
//  AddGeoFence.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-03.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct AddGeoFenceDetails: Codable {
    let subscriberID, geoname, latitude, longitude: String?
    let radius, status, createby: String?

    enum CodingKeys: String, CodingKey {
        case subscriberID = "subscriber_id"
        case geoname, latitude, longitude, radius, status, createby
    }
}
