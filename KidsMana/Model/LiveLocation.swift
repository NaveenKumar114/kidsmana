//
//  LiveLocation.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-06.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation
struct LiveLocation: Codable {
    let code: Int?
    let response: String?
    let employeedetails: Employeedetails?
    let deviceinfo: Deviceinfo?
    let geolocationlist: [Geolocationlist]?
}

// MARK: - Deviceinfo
struct Deviceinfo: Codable {
    let deviceIDPrimary, kidName, kidID, subscriberID: String?
    let isActive, createdOn, createdBy, updatedOn: String?
    let updatedBy, tokenid, mobileNumber, currentposition: String?
    let latitude, longitude, speed, bearing: String?
    let batteryType, batteryPercentage, networkType, networkOperator: String?
    let phoneType, deviceName, deviceManufracture, deviceBrand: String?
    let deviceOS, deviceModel, geofence, weatherinfo: String?

    enum CodingKeys: String, CodingKey {
        case deviceIDPrimary = "device_id_primary"
        case kidName = "kid_name"
        case kidID = "kid_id"
        case subscriberID = "subscriber_id"
        case isActive = "is_active"
        case createdOn = "created_on"
        case createdBy = "created_by"
        case updatedOn = "updated_on"
        case updatedBy = "updated_by"
        case tokenid
        case mobileNumber = "mobile_number"
        case currentposition, latitude, longitude, speed, bearing
        case batteryType = "battery_type"
        case batteryPercentage = "battery_percentage"
        case networkType = "network_type"
        case networkOperator = "network_operator"
        case phoneType = "phone_type"
        case deviceName = "device_name"
        case deviceManufracture = "device_manufracture"
        case deviceBrand = "device_brand"
        case deviceOS = "device_os"
        case deviceModel = "device_model"
        case geofence, weatherinfo
    }
}

// MARK: - Employeedetails
struct Employeedetails: Codable {
    let id, deviceID, name, email: String?
    let icno, mobile, countryCode, designation: String?
    let department, issueDate, gender, password: String?
    let lastLogin, status, role, bannedUsers: String?
    let subscriberID, profile, createdBy, createdDate: String?
    let modifyBy, modifyDate, latitude, longitude: String?
    let profileImage, speed, distance, appiconHideshow: String?
    let tokenid, mobileTime, dailyTimeLimit, mobileTimeEnable: String?
    let deviceAdminEnable: String?

    enum CodingKeys: String, CodingKey {
        case id
        case deviceID = "device_id"
        case name, email, icno, mobile
        case countryCode = "country_code"
        case designation, department
        case issueDate = "issue_date"
        case gender, password
        case lastLogin = "last_login"
        case status, role
        case bannedUsers = "banned_users"
        case subscriberID = "subscriber_id"
        case profile
        case createdBy = "created_by"
        case createdDate = "created_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
        case latitude, longitude
        case profileImage = "profile_image"
        case speed, distance
        case appiconHideshow = "appicon_hideshow"
        case tokenid
        case mobileTime = "mobile_time"
        case dailyTimeLimit = "daily_time_limit"
        case mobileTimeEnable = "mobile_time_enable"
        case deviceAdminEnable = "device_admin_enable"
    }
}

// MARK: - Geolocationlist
struct Geolocationlist: Codable {
    let id, empid, subscriberID, attedate: String?
    let latitude, speed, distance, longitude: String?
    let place: String?
    let status: String?
    let createby: String?
    let createdate: String?
    let modifyby: String?
    let modifydate: String?

    enum CodingKeys: String, CodingKey {
        case id, empid
        case subscriberID = "subscriber_id"
        case attedate, latitude, speed, distance, longitude, place, status, createby, createdate, modifyby, modifydate
    }
}

enum Createby: String, Codable {
    case muhun1 = "MUHUN1"
}

enum Place: String, Codable {
    case klang = "Klang"
    case kualaLumpur = "Kuala Lumpur"
    case petalingJaya = "Petaling Jaya"
    case shahAlam = "Shah Alam"
    case sungaiBuloh = "Sungai Buloh"
}

enum Status: String, Codable {
    case empty = ""
    case entered = "ENTERED"
    case exit = "EXIT"
}
