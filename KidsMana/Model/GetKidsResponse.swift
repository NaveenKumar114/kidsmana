// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct GetKidsResponse: Codable {
    let code: Int?
    let response: String?
    let kids: [Kid]?
}

// MARK: - Kid
struct Kid: Codable {
    let id, deviceID, name, mobile: String?
        let countryCode, password, role, subscriberID: String?
        let profile, createdBy, createdDate, modifyBy: String?
        let modifyDate, latitude, longitude, tokenid: String?
        let appiconHideshow, mobileTime, dailyTimeLimit, mobileTimeEnable: String?
        let deviceAdminEnable, speed, batteryType, batteryPercentage: String?
        let networkType, networkOperator, deviceName, deviceManufracture: String?
        let deviceBrand, deviceOS, deviceModel, weatherinfo: String?
        let geofence: String?

        enum CodingKeys: String, CodingKey {
            case id
                    case deviceID = "device_id"
                    case name, mobile
                    case countryCode = "country_code"
                    case password, role
                    case subscriberID = "subscriber_id"
                    case profile
                    case createdBy = "created_by"
                    case createdDate = "created_date"
                    case modifyBy = "modify_by"
                    case modifyDate = "modify_date"
                    case latitude, longitude, tokenid
                    case appiconHideshow = "appicon_hideshow"
                    case mobileTime = "mobile_time"
                    case dailyTimeLimit = "daily_time_limit"
                    case mobileTimeEnable = "mobile_time_enable"
                    case deviceAdminEnable = "device_admin_enable"
                    case speed
                    case batteryType = "battery_type"
                    case batteryPercentage = "battery_percentage"
                    case networkType = "network_type"
                    case networkOperator = "network_operator"
                    case deviceName = "device_name"
                    case deviceManufracture = "device_manufracture"
                    case deviceBrand = "device_brand"
                    case deviceOS = "device_os"
                    case deviceModel = "device_model"
                    case weatherinfo, geofence
        }
}


// MARK: - Welcome
struct weatherJsonToData: Codable {
    let dt, id, cod: Double?
    let sys: Sys?
    let base: String?
    let main: Main?
    let name: String?
    let rain: Rain?
    let wind: Wind?
    let coord: Coord?
    let clouds: Clouds?
    let weather: [Weather]?
    let timezone, visibility: Double?
}

// MARK: - Clouds
struct Clouds: Codable {
    let all: Double?
}

// MARK: - Coord
struct Coord: Codable {
    let lat, lon: Double?
}

// MARK: - Main
struct Main: Codable {
    let temp, humidity, pressure, tempMax: Double?
    let tempMin: Double?
    let feelsLike: Double?

    enum CodingKeys: String, CodingKey {
        case temp, humidity, pressure
        case tempMax = "temp_max"
        case tempMin = "temp_min"
        case feelsLike = "feels_like"
    }
}

// MARK: - Rain
struct Rain: Codable {
    let the1H: Double?

    enum CodingKeys: String, CodingKey {
        case the1H = "1h"
    }
}

// MARK: - Sys
struct Sys: Codable {
    let id, type, sunset: Double?
    let country: String?
    let sunrise: Double?
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int?
    let icon, main, weatherDescription: String?

    enum CodingKeys: String, CodingKey {
        case id, icon, main
        case weatherDescription = "description"
    }
}

// MARK: - Wind
struct Wind: Codable {
    let deg: Double?
    let speed: Double?
}
