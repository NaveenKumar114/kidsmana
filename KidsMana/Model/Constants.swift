//
//  Constants.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-18.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct imageLink{
    static let profilePicLink = "https://www.kidsmana.com/kidmanawithlatest_api/uploads/kids/profile/IMG-20200425-WA0011.jpg"

}

struct baseUrl {
    static let baseUrl = "https://www.kidsmana.com/"

}

