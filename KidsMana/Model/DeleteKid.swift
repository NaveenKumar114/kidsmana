//
//  DeleteKid.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-09.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation
struct DeleteKid: Codable {
    let code: Int?
    let response: String?
    let kids: [[String: String?]]?
}
