//
//  GeoFenceDetails.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-02.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation
struct GeoFenceDetail: Codable {
    let id, geoname, type, latitude: String
    let longitude, radius, status, subscriberID: String
    let createdate, createby, modifyBy, modifyDate: String
    let userid: String

    enum CodingKeys: String, CodingKey {
        case id, geoname, type, latitude, longitude, radius, status
        case subscriberID = "subscriber_id"
        case createdate, createby
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
        case userid
    }
}

typealias GeoFenceDetails = [GeoFenceDetail]
