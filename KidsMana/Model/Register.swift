//
//  Register.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-11-03.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

// MARK: - Register
struct Register: Codable {
    let code: Int?
    let response: String?
    let companydetails: Companydetails2?
}

// MARK: - Companydetails
struct Companydetails2: Codable {
    let id, name, companyName, email: String?
    let contact, countryCode, address, noEmployees: String?
    let startTime, endTime, status, role: String?
    let profile, password, registrationID, tokenid: String?
    let createdBy, createdDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case companyName = "company_name"
        case email, contact
        case countryCode = "country_code"
        case address
        case noEmployees = "no_employees"
        case startTime = "start_time"
        case endTime = "end_time"
        case status, role, profile, password
        case registrationID = "registration_id"
        case tokenid
        case createdBy = "created_by"
        case createdDate = "created_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
