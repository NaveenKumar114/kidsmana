//
//  AppIconHideShow.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-08.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct AppIconHideShow: Codable {
    let code: Int?
       let response: String?
       let kids: Bool?
}
