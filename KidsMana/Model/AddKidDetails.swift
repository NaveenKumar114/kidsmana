//
//  AddKidDetails.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-01.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

// MARK: - AddKidDetails
struct AddKidDetails: Codable {
    let code: Int
    let response: String
    let kid: KidDetails
}

// MARK: - Kid
struct KidDetails: Codable {
    let subscriberID, name, role: String
    let appiconHideshow: Int
    let mobile, countryCode, password, modifyBy: String
    let createdBy, modifyDate, createdDate: String

    enum CodingKeys: String, CodingKey {
        case subscriberID = "subscriber_id"
        case name, role
        case appiconHideshow = "appicon_hideshow"
        case mobile
        case countryCode = "country_code"
        case password
        case modifyBy = "modify_by"
        case createdBy = "created_by"
        case modifyDate = "modify_date"
        case createdDate = "created_date"
    }
}
