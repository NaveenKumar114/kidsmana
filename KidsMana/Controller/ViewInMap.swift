//
//  ViewInMap.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-19.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import MapKit
import DatePicker
class ViewInMap: UIViewController {
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var chargingLabel: UILabel!
    @IBOutlet weak var batteryPaercent: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var deviceType: UILabel!
    @IBOutlet weak var networkType: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var bottomCollectionView: UICollectionView!
    @IBOutlet weak var multiClose: UIImageView!
    @IBOutlet weak var speed: UILabel!
    @IBOutlet weak var kmphLabel: UILabel!
    var annotionSet = 0
    @IBOutlet weak var speedLabel: UILabel!
    let locationManager = CLLocationManager()
    var pointAnnotation: [CustomPointAnnotation] = []
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var dateButtonTop: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var goButton: UIButton!
    var polylineRenderer : MKPolylineRenderer?
    @IBOutlet weak var forMultiple: UIView!
    @IBOutlet weak var middleContentView: UIView!
    @IBOutlet weak var topContentView: UIView!
    @IBOutlet weak var closeImage: UIImageView!
    @IBOutlet weak var callImageView: UIImageView!
    var latitudes : [String] = []
    var lat : String?
    var long : String?
    var longitudes : [String] = []
    var employeeData : [Employeelist] = []
    var countForSingle = 0
    var currentSelected : String?
    var timer = Timer()
    var phoneNumber : String?
    var currentTime :[String] = []
    var status : [Bool] = []
    var selectionState : [Bool] = []
    var missingPoint : [Int] = []
    var centerCorordinated : CLLocationCoordinate2D?
    var liveLocationData : [CLLocationCoordinate2D] = []
    var day = "0"
    var month = "0"
    var year = "0"
    var totalDistance = 0.0
    @objc func segueToDashboard()
    {
        print("saa")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        day = String(date.day())
        month = String(date.month())
        year = String(date.year())
        print(day,month,year)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        dateButton.setTitle(result, for: .normal)
        dateButtonTop.setTitle(result, for: .normal)
        topContentView.isHidden = true
        middleContentView.isHidden = true
        forMultiple.isHidden = false
        bottomCollectionView.register(UINib(nibName: "ViewInMapColloectionView", bundle: nil), forCellWithReuseIdentifier: "ViewInMapColloectionView")
        bottomCollectionView.delegate = self
        bottomCollectionView.dataSource = self
        kmphLabel.font = UIFont(name: "Digital-7 Italic", size: 14)!
        speedLabel.font = UIFont(name: "Digital-7 Italic", size: 35)!
        mapView.delegate = self
        topContentView.backgroundColor = UIColor(white: 1, alpha: 0.8)
        dateButton.makeRounded()
        
        goButton.makeRounded()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(segueToDashboard))
        closeImage.isUserInteractionEnabled = true
        self.closeImage.addGestureRecognizer(gesture)
        let gestureImage = UITapGestureRecognizer(target: self, action:  #selector(phone))
        callImageView.isUserInteractionEnabled = true
        self.callImageView.addGestureRecognizer(gestureImage)
        let multiGesture =  UITapGestureRecognizer(target: self, action:  #selector(segueToDashboard))
        self.multiClose.addGestureRecognizer(multiGesture)
        makeGetCall()
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(checkUser) , userInfo: nil, repeats: true)
    }
    @objc func phone()
    {
        if let p = phoneNumber
        {
            dialNumber(number: p)
        }
        else
        {
            let alert = UIAlertController(title: "Phone", message: "No number available", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
    }
    @IBAction func goButtonPressed(_ sender: Any) {
        openMapForPlace(lat: Double(lat!)!, long: Double(long!)!)
      
    }
    func openMapForPlace(lat: Double , long: Double) {

          
        let coordinates = CLLocationCoordinate2D(latitude:lat
                                , longitude:long)
           let regionDistance:CLLocationDistance = 10000
           //let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
           let options = [
               MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
               MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
           ]
           let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
           let mapItem = MKMapItem(placemark: placemark)
           mapItem.name = "Place Name"
           mapItem.openInMaps(launchOptions: options)
       }
    @IBAction func dateButtonClicked(_ sender: Any) {
        let minDate = DatePickerHelper.shared.dateFrom(day: 18, month: 08, year: 1990)!
        let maxDate = DatePickerHelper.shared.dateFrom(day: 18, month: 08, year: 2030)!
        let today = Date()
        // Create picker object
        let datePicker = DatePicker()
        // Setup
        datePicker.setup(beginWith: today, min: minDate, max: maxDate) { (selected, date) in
            if selected, let selectedDate = date {
                //   print(selectedDate.string())
                self.month = String(selectedDate.month())
                self.year = String(selectedDate.year())
                self.day = String(selectedDate.day())
                self.dateButton.setTitle("\(selectedDate.day()).\(selectedDate.month()).\(selectedDate.year())", for: .normal)
                self.dateButtonTop.setTitle("\(selectedDate.day()).\(selectedDate.month()).\(selectedDate.year())", for: .normal)
                self.liveLocation()
            } else {
                print("Cancelled")
            }
        }
        // Display
        datePicker.show(in: self, on: sender as? UIView)
    }
    @objc func checkUser()
    {
        centerCorordinated = mapView.centerCoordinate
        
        checkLocation()
        // bottomCollectionView.reloadData()
        
    }
    @IBOutlet weak var closeButtonPressed: UIImageView!
    @IBAction func liveLocationButton(_ sender: Any) {
        // addAnnotion()
    }
    func checkLocation()
    {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/getAllemployeeById/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let locationData = try? decoder.decode(AllLocation.self, from: data!)
                
                // print(String(data: data!, encoding: String.Encoding.utf8))
                
                let code_str = locationData?.code
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        print("success")
                        mapView.removeAnnotations(pointAnnotation)
                        pointAnnotation.removeAll()
                        latitudes.removeAll()
                        longitudes.removeAll()
                        for n in 0...(locationData?.employeelist.count)! - 1
                        {
                            //print(locationData?.employeelist[n].modifyDate)
                            let currentModify = locationData?.employeelist[n].modifyDate
                            let delimiter = " "
                            let token = currentModify!.components(separatedBy: delimiter)
                            //print(token[1])
                            if currentTime[n] != token[1]
                            {
                                //   print(currentTime[n])
                                //  print(token[1])
                                //    print("online")
                                status[n] = true
                                currentTime[n] = token[1]
                                let cell = bottomCollectionView.cellForItem(at: IndexPath(row: n, section: 0)) as! ViewInMapColloectionView
                                cell.profileImage.layer.borderColor = UIColor.green.cgColor
                                
                            }
                            else
                            {
                                let cell = bottomCollectionView.cellForItem(at: IndexPath(row: n, section: 0)) as! ViewInMapColloectionView
                                cell.profileImage.layer.borderColor = UIColor.red.cgColor
                                status[n] = false
                                //     print(currentTime[n])
                                //    print(token[1])
                                //   print("offlibe")
                            }
                            let lat = locationData?.employeelist[n].latitude
                            let long = locationData?.employeelist[n].longitude
                            let x : CustomPointAnnotation = CustomPointAnnotation()
                            pointAnnotation.append(x)
                            latitudes.append(lat!)
                            longitudes.append(long!)
                        }
                        updateAnnotion()
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: localizedStrings.kidsLocation, message: localizedStrings.locationNotFound, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                }
                
            }
            
            
        }
        task.resume()
        
    }
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/getAllemployeeById/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let locationData = try? decoder.decode(AllLocation.self, from: data!)
                
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                let code_str = locationData?.code
              
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        print("success")
                        for n in 0...(locationData?.employeelist.count)! - 1 {
                            let lat = locationData?.employeelist[n].latitude
                            let long = locationData?.employeelist[n].longitude
                            let x : CustomPointAnnotation = CustomPointAnnotation()
                            pointAnnotation.append(x)
                            latitudes.append(lat!)
                            longitudes.append(long!)
                            employeeData.append((locationData?.employeelist[n])!)
                            //  currentTime.append((locationData?.employeelist[n].modifyDate)!) let currentModify = locationData?.employeelist[n].modifyDate
                            let currentModify = locationData?.employeelist[n].modifyDate
                            print("check \(currentModify)")
                            let delimiter = " "
                            let token = currentModify!.components(separatedBy: delimiter)
                            currentTime.append(token[1])
                            print(token[1])
                            status.append(true)
                        }
                        addAnnotion()
                        bottomCollectionView.reloadData()
                        countForSingle = employeeData.count
                        print(employeeData.count)
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: localizedStrings.kidsLocation, message: localizedStrings.locationNotFound, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
    func liveLocationTrackingApiCAll()
    {
        time.text = "0:00"
        speedLabel.text = "0"
        chargingLabel.text = ""
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/postGeolocationlist/employee/postGeolocationlist/")! as URL)
        request.httpMethod = "POST"
        let date = "\(year)-\(month)-\(day)"
        let postString = "emp_id=\(currentSelected!)&subscriber_id=\(id)&date=\(date)&modifydate="
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let locationData = try? decoder.decode(LiveLocation.self, from: data!)
                
                // print(String(data: data!, encoding: String.Encoding.utf8))
                
                let code_str = locationData?.code
                // print(locationData?.response)
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        print("success")
                        if locationData?.deviceinfo?.weatherinfo != nil
                        {
                            let data2 = locationData?.deviceinfo?.weatherinfo!.data(using: .utf8)
                            let x = try? decoder.decode(weatherJsonToData.self, from: data2!)
                            //print(String(data: data2!, encoding: String.Encoding.utf8))
                            // print(x)
                            // print("yes")
                            if x != nil
                            {
                                
                                let temp = x?.main?.temp
                                temperature.text = String(temp!)
                                let name = x?.name
                                let country = x?.sys?.country
                                locationLabel.text = "\(name!) , \(country!)"
                            }
                            
                        }
                        profileName.text = locationData?.employeedetails?.name
                        let deviceMan = locationData?.deviceinfo?.deviceManufracture
                        let deviceName = locationData?.deviceinfo?.deviceName
                        if deviceMan != nil && deviceName != nil
                        {
                        deviceType.text = "\(deviceMan!) \(deviceName!)"
                        }
                        
                        let d = locationData?.employeedetails?.distance
                        if d != nil && d != ""
                        {
                        let d1 = d!.components(separatedBy: ".")
                        let dec = d1[1]
                        
                        distance.text = "\(d1[0]).\(dec.prefix(4))"
                        }
                        let s = Double((locationData?.employeedetails?.speed)!)
                        if s != nil
                        {
                        speed.text = String(s!.rounded(toPlaces: 2))
                        }
                        if locationData?.deviceinfo?.batteryType != "DISCHARGING"
                        {
                            chargingLabel.text = "Charging"
                        }
                        
                        let perc = locationData?.deviceinfo?.batteryPercentage
                        if perc != nil && perc != ""
                        {
                        batteryPaercent.text = " \(perc!)%"
                        }
                        let nt = locationData?.deviceinfo?.networkType
                        let no = locationData?.deviceinfo?.networkOperator
                        if nt != nil && nt != "" && no != nil && no != ""
                        {
                        networkType.text = "\(nt!) \(no!)"
                        }
                        liveLocationData.removeAll()
                        if locationData?.geolocationlist != nil
                        {
                            mapView.removeOverlays(mapView.overlays)
                            for n in 0...(locationData?.geolocationlist?.count)! - 1
                            {
                                let dis = Double((locationData?.geolocationlist?[n].distance)!)
                                totalDistance = totalDistance + dis!
                                let lat = Double((locationData?.geolocationlist?[n].latitude)!)
                                let long = Double((locationData?.geolocationlist?[n].longitude)!)
                                liveLocationData.append(CLLocationCoordinate2D(latitude: lat!, longitude: long!))
                            }
                            let firstLocation = locationData?.geolocationlist?.first?.createdate
                            let lastLocation = locationData?.geolocationlist?.last?.createdate
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?

                            let firstDate = dateFormatter.date(from: (firstLocation!))
                            let lastDate = dateFormatter.date(from: (lastLocation!))
                            let interval = lastDate?.timeIntervalSince(firstDate!)
                            let s = Double((locationData?.geolocationlist?.last?.speed)!)
                            if s != nil
                            {
                            speed.text = String(s!.rounded(toPlaces: 2))
                            }
                            let timeStr = interval?.stringFromTimeInterval()
                            time.text = timeStr!
                            distance.text = String(totalDistance.rounded(toPlaces: 2))
                            drawPolyline()
                            totalDistance = 0.0
                        }
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: localizedStrings.kidsLocation, message: localizedStrings.locationNotFound, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                }
            }
        }
        task.resume()
        
    }
    func liveLocation()
    {
        for n in 0...selectionState.count - 1
        {
            if selectionState[n] == false
            {
                lat  = employeeData[n].latitude
                long = employeeData[n].longitude
                phoneNumber = employeeData[n].mobile
                currentSelected = String(employeeData[n].id!)
            }
        }
        liveLocationTrackingApiCAll()
        
    }
    func drawPolyline()
    {
        
        //let livePolyline = MKPolyline(coordinates: liveLocationData, count: liveLocationData.count)
        let geodesic = MKGeodesicPolyline(coordinates: liveLocationData, count: liveLocationData.count)
        mapView.addOverlay(geodesic)
        
    }
}

extension UIButton
{
    func makeRounded()
    {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
    }
}


extension ViewInMap : MKMapViewDelegate,  CLLocationManagerDelegate
{
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer!.strokeColor = UIColor.blue
            polylineRenderer!.lineWidth = 5
            return polylineRenderer
        }
        
        return nil
    }
    
    func addAnnotion()
    {
        print(latitudes.count)
        for n in 0...latitudes.count - 1
        {
            
           if  let lat = Double(latitudes[n])
           {
           if let long = Double(longitudes[n])
           {
            print(lat , long)
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //pointAnnotation = CustomPointAnnotation()
            pointAnnotation[n].pinCustomImageName = "online_marker"
            pointAnnotation[n].coordinate = location
            pointAnnotation[n].title = employeeData[n].name
            pointAnnotation[n].subtitle = "Current Location"
            pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation[n], reuseIdentifier: "pin")
            
            mapView.addAnnotation(pinAnnotationView.annotation!)
            mapView.centerCoordinate = location
            mapView.showAnnotations(self.mapView.annotations, animated: true)
           }
           }
        }
        centerCorordinated = mapView.centerCoordinate
    }
    func updateAnnotion()
    {
        print(latitudes.count)
        for n in 0...latitudes.count - 1
        {
            if latitudes[n] != "" && longitudes[n] != ""
            {
            let lat = Double(latitudes[n])!
            let long = Double(longitudes[n])!
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            //pointAnnotation = CustomPointAnnotation()
            //  print(n)
            // print(status.count)
            if status[n] == true
            {
                pointAnnotation[n].pinCustomImageName = "online_marker"
                
            }
            else
            {
                pointAnnotation[n].pinCustomImageName = "offline_marker"
            }
            pointAnnotation[n].coordinate = location
            pointAnnotation[n].title = employeeData[n].name
            pointAnnotation[n].subtitle = "Current Location"
            pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation[n], reuseIdentifier: "pin")
            
            if selectionState[n] == false
            {
                print("true")
                mapView.addAnnotation(pinAnnotationView.annotation!)
                mapView.showAnnotations(self.mapView.annotations, animated: true)
                
            }
            else
            {
                missingPoint.append(n)
            }
            }
            
        }
        mapView.setCenter(centerCorordinated!, animated: true)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
        
        return annotationView
    }
}

extension ViewInMap : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeeData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = bottomCollectionView.dequeueReusableCell(withReuseIdentifier: "ViewInMapColloectionView", for: indexPath) as! ViewInMapColloectionView
        cell.profileName.text = "dada"
        cell.profileName.text = employeeData[indexPath.row].name
        if selectionState.count > indexPath.row
        {
            if selectionState[indexPath.row] == true
            {
                //   print("true")
                cell.alpha = 0.5
            }
            else
            {
                //    print("false")
                cell.alpha = 1
            }
        }
        else
        {
            selectionState.append(false)
        }
        if employeeData[indexPath.row].profile != "" && employeeData[indexPath.row].profile != nil
        {
            let profile = employeeData[indexPath.row].profile
            let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(profile!)")
            //  print(urlStr)
            let url = URL(string: urlStr)
            if  let data = try? Data(contentsOf: url!) {
                cell.profileImage.image = UIImage(data: data)
                
            }
            if status[indexPath.row] == true
            {
                cell.profileImage.makeRounded(color: UIColor.green.cgColor , borderwidth: 4)
            }
            else
            {
                cell.profileImage.makeRounded(color: UIColor.red.cgColor , borderwidth: 4)
                
            }
            if selectionState.count > indexPath.row
            {
                if selectionState[indexPath.row] == true
                {
                    // print("true")
                    cell.alpha = 0.5
                }
                else
                {
                    //  print("false")
                    cell.alpha = 1
                }
            }
            else
            {
                selectionState.append(false)
            }
        }
        //     print(indexPath.row)
        //    print(cell.alpha)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = bottomCollectionView.cellForItem(at: indexPath) as! ViewInMapColloectionView
        
        if selectionState[indexPath.row] == false
        {
            mapView.view(for: pointAnnotation[indexPath.row])?.isHidden = true
            cell.alpha = 0.5
            selectionState[indexPath.row] = true
            countForSingle -= 1
            
        }
        else
        {
            if missingPoint.count != 0
            {
                for n in 0...missingPoint.count - 1
                {
                    if missingPoint[n] == indexPath.row
                    {
                        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation[indexPath.row], reuseIdentifier: "pin")
                        mapView.addAnnotation(pinAnnotationView.annotation!)
                        mapView.showAnnotations(self.mapView.annotations, animated: true)
                        missingPoint.remove(at: n)
                        break
                    }
                    
                    
                    
                }
            }
            mapView.view(for: pointAnnotation[indexPath.row])?.isHidden = false
            cell.alpha = 1
            selectionState[indexPath.row] = false
            countForSingle += 1
            
        }
        print(countForSingle)
        if countForSingle == 0
        {
            mapView.removeOverlays(mapView.overlays)
            
            forMultiple.isHidden = true
            topContentView.isHidden = true
            middleContentView.isHidden = true
        }
        else
        if countForSingle == 1
        {
            liveLocation()
            forMultiple.isHidden = true
            topContentView.isHidden = false
            middleContentView.isHidden = false
        }
        else
        {
            mapView.removeOverlays(mapView.overlays)
            forMultiple.isHidden = false
            topContentView.isHidden = true
            middleContentView.isHidden = true
        }
        
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension TimeInterval{

        func stringFromTimeInterval() -> String {

            let time = NSInteger(self)

            let seconds = time % 60
            let minutes = (time / 60) % 60
            let hours = (time / 3600)

            return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)

        }
    }
