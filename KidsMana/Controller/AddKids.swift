//
//  AddKids.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-18.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import CountryPickerView
class AddKids: UIViewController, CountryPickerViewDelegate, CountryPickerViewDataSource, UITextFieldDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
       // print(country)
        countryCode = country.phoneCode
        print(countryCode)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           self.view.endEditing(true)
           return false
       }
    
    @IBOutlet weak var saveButton: UIButton!
    var countryCode = "+1"
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logoCircle: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var mobileNoField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    var password : String?
    var mobile : String?
    var name : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordField.delegate = self
        mobileNoField.keyboardType = .numberPad
        mobileNoField.delegate = self
        nameField.delegate = self
        passwordField.isSecureTextEntry = true
        logoCircle.rotate360Degrees()
        logo.dropShadow(scale: true)
        
        //nameField.dropShadow()
        nameField.addShadowToTextField(cornerRadius: 10)
        passwordField.addShadowToTextField(cornerRadius: 10)
        mobileNoField.addShadowToTextField(cornerRadius: 10)
         let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        
           //cpv.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        //   mobileNo.addShadowToTextField(cornerRadius: 3)
          // passwordTextField.addShadowToTextField(cornerRadius: 3)
           cpv.delegate = self
           cpv.dataSource = self
           cpv.font = UIFont(name: "Arial-Rounded", size: 12)!
           cpv.textColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
      saveButton.layer.cornerRadius = 5
           mobileNoField.leftView = cpv
          // mobileNo.leftView = paddingView
           mobileNoField.leftViewMode = .always
        countryCode = cpv.selectedCountry.phoneCode

    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        if nameField.text == nil
        {
            let alert = UIAlertController(title: localizedStrings.addNewKids, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        if passwordField.text == nil {
            let alert = UIAlertController(title: localizedStrings.addNewKids, message: localizedStrings.enterPassword, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        if mobileNoField.text == nil || mobileNoField.text!.count != 10{
            let alert = UIAlertController(title: localizedStrings.addNewKids, message: localizedStrings.enterMobile, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        {
            name = nameField.text
            password = passwordField.text
            mobile = mobileNoField.text
            makePostCall()
            showSpinner(onView: saveButton)
        }
        
    }
    
 func makePostCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/add_update_kid/")! as URL)
        request.httpMethod = "POST"
        let pass = password!
        let mob = mobile!
        let nam = name!
        let id = UserDefaults.standard.string(forKey: "id")!
        let parentName = UserDefaults.standard.string(forKey: "userName")!
        let postString = "subscriber_id=\(id)&parent_id\(id)&name=\(nam)&ageStr=\(nam)&dobStr=\(nam)&education=\(nam)&mobile=\(mob)&password=\(pass)&cerateby=\(parentName)&modifyby=\(parentName)&country_code=\(countryCode)&upload_type=\(2)"
    let content = postString.replacingOccurrences(of: "+", with: "%2B")
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = content.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
               
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddKidDetails.self, from: data!)
            
              
                let code_str = loginBaseResponse?.code
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                DispatchQueue.main.async {
                
                    if code_str == 200 {
                        self.removeSpinner()
                        print("success")
                        NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                        let alert = UIAlertController(title: localizedStrings.addNewKids, message: localizedStrings.kidAddedSuccessfully, preferredStyle: UIAlertController.Style.alert)

                                // add an action (button)
                                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    else
                    {
                        self.removeSpinner()
                        let alert = UIAlertController(title: localizedStrings.addNewKids, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)

                                // add an action (button)
                                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                    }
                
                }
                
                
            }
//                catch {
//                    self.removeSpinner()
//                    print("Error -> \(error)")
//                    showAlert(title: "Server Error", message: "Try Again later")
//                }
        }
       task.resume()
    }
    
}

