//
//  Test.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-17.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import SOTabBar
class DashboardTabController: SOTabBarController, SOTabBarControllerDelegate  {
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
        print("da")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "dashboard")
        let secondVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addKids")
        let thirdVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "kidslist")
        firstVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "home_icon"), selectedImage: UIImage(named: "home_icon"))
        secondVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "plus_128"), selectedImage: UIImage(named: "plus_128"))
        thirdVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "newkid_icon"), selectedImage: UIImage(named: "newkid_icon"))
        viewControllers = [firstVC, secondVC,thirdVC]
      
        
    }
    override func loadView() {
           super.loadView()
           SOTabBarSetting.tabBarBackground = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
        SOTabBarSetting.tabBarTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        SOTabBarSetting.tabBarShadowColor = UIColor.black.cgColor
       }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
