//
//  Settings.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-17.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class Settings: UIViewController {
    var settingsMenuItems = [localizedStrings.home , localizedStrings.addKid , localizedStrings.kidsList , localizedStrings.appTimeLimit , localizedStrings.liveTracking , localizedStrings.geoFence , "Go Premium" , "Language" , "About Us" , "Refund Policy" , "Shipping/Fulfillment Policy" , "Privacy" , localizedStrings.logOut]
    var settingsIconNames = ["001-home" , "002-add-group" , "003-family" , "004-clock" , "005-delivery" , "006-add-location-point" , "007-quality" , "008-translate" , "info" , "refund", "agreement" , "privacy" , "009-logout"]
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var settingsItem: UITableView!
    @IBOutlet weak var profileName: UILabel!
    var kidsData : [Kid] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsItem.register(UINib(nibName: "SettingsCellTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsCellTableViewCell")
        settingsItem.delegate = self
        settingsItem.dataSource = self
        profileImageView.makeRounded(color: UIColor.white.cgColor , borderwidth: 2)
        let defaults = UserDefaults.standard
        let imgStr = defaults.string(forKey: "img")
        if imgStr != nil
        {
        let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(imgStr!)")
        print(urlStr)
        let url = URL(string: urlStr)
        if  let data = try? Data(contentsOf: url!){
            let img = UIImage(data: data)
            profileImageView.image = img
        }
        }
        profileName.text =  UserDefaults.standard.string(forKey: "userName")
        print(kidsData.count)
    }
}


extension Settings : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCellTableViewCell", for: indexPath) as! SettingsCellTableViewCell
        cell.menuLabel.text = settingsMenuItems[indexPath.row]
        cell.menuImage.image = UIImage(named: settingsIconNames[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
               (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 1:

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
               (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
               (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 3:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "timeLimit") as AppTimeLimit
            if kidsData.count > 0
            {
                for n in 0...kidsData.count - 1
                {
                    mainTabBarController.kidsName.append(kidsData[n].name!)
                    mainTabBarController.kidsId.append(kidsData[n].id!)
                }
            }
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 4:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "viewLocation")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 5:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "geofence")
         (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        case 6:
            let storyboard = UIStoryboard(name: "SelectSubscription", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "selectSubscription")

              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
            
        case 7:
            let alert = UIAlertController(title: "Language", message: "Language Page", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        case 8:
            let storyboard = UIStoryboard(name: "AboutUs", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "aboutUs")

              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        case 9:
            let storyboard = UIStoryboard(name: "RefundPolicy", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "refundPolicy") as! RefundPolicy
            loginNavController.type = "refund"
              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        case 10:
            let storyboard = UIStoryboard(name: "RefundPolicy", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "refundPolicy") as! RefundPolicy
            loginNavController.type = "tos"
              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        case 11:
            let storyboard = UIStoryboard(name: "RefundPolicy", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "refundPolicy") as! RefundPolicy
            loginNavController.type = "privacy"
              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        case 12:
            let defaults = UserDefaults.standard
            defaults.setValue("false", forKey: "isLoggedIn")
            defaults.removeObject(forKey: "email")
            defaults.removeObject(forKey: "userName")
            NotificationCenter.default.post(name: Notification.Name("logout"), object: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let loginNavController = storyboard.instantiateViewController(identifier: "mainNavi")

              (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        default:
            print("error")
            print(indexPath.row)
        }
    }
}
