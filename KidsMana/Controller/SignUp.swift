//
//  SignUp.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-26.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//
import Alamofire
import UIKit
import CountryPickerView
class SignUp: UIViewController, CountryPickerViewDelegate, CountryPickerViewDataSource, UITextFieldDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        countryCode = country.phoneCode
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    var countryCode : String?

    var profileImage : UIImage?
    @IBOutlet weak var circle: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var mobileNoText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var subscribeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        
        cpv.delegate = self
        cpv.dataSource = self
        cpv.font = UIFont(name: "Arial-Rounded", size: 12)!
        cpv.textColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
        //   let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        mobileNoText.leftView = cpv
        // mobileNo.leftView = paddingView
        logo.dropShadow()
        confirmPassword.delegate = self
        passwordText.delegate = self
        mobileNoText.delegate = self
        emailText.delegate = self
        nameText.delegate = self
        circle.rotate360Degrees()
        mobileNoText.leftViewMode = .always
        //mobileNoText.keyboardType = .numberPad
        passwordText.isSecureTextEntry = true
        confirmPassword.isSecureTextEntry = true
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 1
        subscribeButton.layer.cornerRadius = 5
        subscribeButton.layer.borderWidth = 1
        let gesture = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        logo.isUserInteractionEnabled = true
        logo.addGestureRecognizer(gesture)
        countryCode = cpv.selectedCountry.phoneCode
    }
    @objc func selectImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: "login")
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    
    @IBAction func subscribeButtonPressed(_ sender: Any) {
      
        if nameText.text == ""
        {
            let alert = UIAlertController(title: "Register", message: "Invalid Name", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if passwordText.text == "" || passwordText.text != confirmPassword.text{
            let alert = UIAlertController(title: "Register", message: "Invalid Password", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if passwordText.text!.count < 4
        {
            let alert = UIAlertController(title: "Register", message: "Password must be minumum 4 characters", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if mobileNoText.text == "" || mobileNoText.text!.count > 11 || mobileNoText.text!.count < 6 {
            let alert = UIAlertController(title: localizedStrings.register, message: localizedStrings.enterMobile, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if emailText.text?.isValidEmail == false
        {
            let alert = UIAlertController(title: localizedStrings.register, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
       else
        {
            
            if profileImage != nil
            {
                let name = nameText.text
                let email = emailText.text
                let password = passwordText.text
                let mobileNo = mobileNoText.text
                let date = NSDate()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                formatter.timeZone = TimeZone(abbreviation: "UTC")
                let defaultTimeZoneStr = formatter.string(from: date as Date)
                let modifyDate = defaultTimeZoneStr
                let two = nameText.text!.prefix(2)
                var fourDigitNumber: String {
                 var result = ""
                 repeat {
                     // Create a string with a random number 0...9999
                     result = String(format:"%05d", arc4random_uniform(100000) )
                    print(result)
                 } while Set<Character>(result).count < 5
                 return result
                }
                let reg = "k-\(two)\(fourDigitNumber)"
                let regUpp = reg.uppercased()
                //let content = countryCode!.replacingOccurrences(of: "+", with: "%2B")
             
                let parameter : Parameters = ["name" : name! , "email" : email! , "contact" : mobileNo! , "password" : password! , "company_name" : name! , "country_code" : countryCode! , "address" : "" , "start_time" : "09:00:00" , "no_employees" : "4" , "created_by" : name! , "modify_by" : name!  , "upload_type" : 1 , "end_time" : "19:00:00" , "status" : "pending" , "role" : "1" , "registration_id" : regUpp , "tokenid" : name! , "modify_date" : modifyDate , "created_date" : modifyDate]
                let imageData = profileImage!.jpegData(compressionQuality: 0.50)
                //print(parameter)
               
              uploadImage(image: imageData!, to: URL(string: "\(baseUrl.baseUrl)api/employee/admin_register")!, params: parameter)
            }
            else
            {
                makePostCall()
            }
        }
        
    }
    
     func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
               /* if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                    print(multipart)
                } */
                if let data = value.removingPercentEncoding?.data(using: .utf8) {
                   // do your stuff here
                    multipart.append(data, withName: key)
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile.png", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(Register.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                self.removeSpinner()
                                print("success")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let loginNavController = storyboard.instantiateViewController(identifier: "login")
                                
                                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
                                // NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                               
                                
                                
                            }
                            else
                            {
                                print(code_str as Any)
                                self.removeSpinner()
                                let alert = UIAlertController(title: localizedStrings.register, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
        }
        
    
    }
        
 func makePostCall() {
    print("enterpost")
    let name = nameText.text
    let email = emailText.text
    let password = passwordText.text
    let mobileNo = mobileNoText.text
    let date = NSDate()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    let defaultTimeZoneStr = formatter.string(from: date as Date)
    let modifyDate = defaultTimeZoneStr
    let two = nameText.text!.prefix(2)
    var fourDigitNumber: String {
     var result = ""
     repeat {
         // Create a string with a random number 0...9999
         result = String(format:"%05d", arc4random_uniform(100000) )
        print(result)
     } while Set<Character>(result).count < 5
     return result
    }
    let reg = "k-\(two)\(fourDigitNumber)"
    let regUpp = reg.uppercased()
     let decoder = JSONDecoder()
     let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/admin_register")! as URL)
     request.httpMethod = "POST"
     let postString = "name=\(name!)&email=\(email!)&contact=\(mobileNo!)&password=\(password!)&company_name=\(name!)&country_code=\(countryCode!)&address=\("")&start_time=\("09:00:00")&no_employees=\("4")&end_time=\("19:00:00")&status=\("pending")&role=\("1")&registration_id=\(regUpp)&tokenid=\(name!)&created_by=\(name!)&modify_by=\(name!)&modify_date=\(modifyDate)&created_date=\(modifyDate)&upload_type=\(2)"
     //let content = postString.replacingOccurrences(of: "+", with: "%2B")
    // print(content)
     request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
     request.httpBody = postString.data(using: String.Encoding.utf8)
     
     let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
     guard error == nil && data != nil else {                                                          // check for fundamental networking error
     print("error=\(String(describing: error))")
     
     return
     }
     do {
     
     let loginBaseResponse = try? decoder.decode(Register.self, from: data!)
     
     
     let code_str = loginBaseResponse?.code
     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
     
     DispatchQueue.main.async {
     
     if code_str == 200 {
     self.removeSpinner()
     print("success")
        print(loginBaseResponse)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: "login")
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
     
     
     
     }
     else
     {
        print(loginBaseResponse)
     self.removeSpinner()
        let alert = UIAlertController(title: localizedStrings.register, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
     
     // add an action (button)
        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
     
     // show the alert
     self.present(alert, animated: true, completion: nil)
     }
     
     }
     
     
     }
    
     }
     task.resume()
     }
    
}

extension SignUp : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {
        profileImage = image
        logo.image = image
        logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
        
    }
}

extension String {
    var isValidEmail: Bool {
        let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
        return testEmail.evaluate(with: self)
    }
}
