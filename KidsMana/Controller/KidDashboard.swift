//
//  KidDashboard.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-13.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//
import SystemConfiguration
import QuartzCore
import UIKit
import CoreLocation
import CoreTelephony
import UserNotifications
import Firebase
import FirebaseAuth
class KidDashboard: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var parentProfilePicture: UIImageView!
    var locationManager: CLLocationManager?
    var timer = Timer()
    @IBOutlet weak var kidProfilePickture: UIImageView!
    @IBOutlet weak var kidNameLabel: UILabel!
    let t = RepeatingTimer(timeInterval: 10)
    var time = 3
    var flag = 0
    var kilFlag = 0
    var currentLocation : CLLocation?
    var previousLocation : CLLocation?
    var geoFenceItems : [GeoFenceDetail]?
    var geoFenceUpdateData = geoFenceUpdate()
    
    @objc func logout()
    {
        t.cancel()
        print("logout")
    }
    func sendNotification()
    {
        print("notif")
        let content = UNMutableNotificationContent()
        content.title = "Feed the cat"
        content.subtitle = "It looks hungry"
        content.sound = UNNotificationSound.default
        
        // show this notification five seconds from now
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        // choose a random identifier
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        // add our notification request
        UNUserNotificationCenter.current().add(request)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        /*    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
         if success {
         self.sendNotification()
         
         print("All set!")
         } else if let error = error {
         print(error.localizedDescription)
         }
         } */
        t.eventHandler = { [self] in
            print("Timer Fired")
            time = time + 3
            print(Date())
            flag = 1
            locationManager?.requestLocation()
            print(UIApplication.shared.backgroundTimeRemaining)
        }
        t.resume()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.requestLocation()
        locationManager?.startUpdatingLocation()
        //timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(location) , userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(logout), name: Notification.Name("logout"), object: nil)
        
        let defaults = UserDefaults.standard
        let imgStr = defaults.string(forKey:"imgKid")
        if imgStr != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(imgStr!)")
            print(urlStr)
            
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    kidProfilePickture.image = UIImage(data: data!)
                    kidProfilePickture.contentMode = .scaleAspectFill
                    
                    kidProfilePickture.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 2)
                }
                
            }
        }
        
        let imgStrParent = defaults.string(forKey:"img")
        if imgStrParent != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(imgStrParent!)")
            print(urlStr)
            
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    parentProfilePicture.image = UIImage(data: data!)
                    parentProfilePicture.contentMode = .scaleAspectFill
                    
                    parentProfilePicture.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 2)
                }
                
            }
        }
        let name = defaults.string(forKey: "userNameKid")
        
        kidNameLabel.text = name!
        if let n = defaults.string(forKey: "test")
        {
            print(n)
            kidNameLabel.text = n
        }
        defaults.removeObject(forKey: "test")
        startMySignificantLocationChanges()
    }
    
    
    func startMySignificantLocationChanges() {
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            
            let alert = UIAlertController(title: "Alert", message: "SignificantLocationChanges not supported", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            return
        }
        else
        {
            print("supported")
        }
        locationManager!.startMonitoringSignificantLocationChanges()
    }
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    @objc func location()
    {
        locationManager?.requestLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
        }
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        geoFenceUpdateData.name = region.identifier
        geoFenceUpdateData.stauts = "ENTRY"
        geoFenceUpdateData.state = true
        print("Entered  \(region)")
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        geoFenceUpdateData.name = region.identifier
        geoFenceUpdateData.stauts = "EXIT"
        geoFenceUpdateData.state = true
        print("Exitied   \(region)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if flag == 1
        {
            flag = 0
            if UIApplication.shared.applicationState  == .active
            {
                print("active")
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                    let distance = currentLocation?.distance(from: previousLocation!)
                    
                    print(Double(distance!) / 1000)
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
                    getAddressFromLatLon(pdblLatitude: String(location.coordinate.latitude), withLongitude: String(location.coordinate.longitude) , speed: String(location.speed), accuracy: location.horizontalAccuracy, distance: String(distance!))
                }
            }
            if UIApplication.shared.applicationState == .active {
                print("da")
            } else {
                print("App is backgrounded. New location is %@", locations.last as Any)
                print(locations.first as Any)
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                    let distance = currentLocation?.distance(from: previousLocation!)
                    
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
                    getAddressFromLatLon(pdblLatitude: String(location.coordinate.latitude), withLongitude: String(location.coordinate.longitude) , speed: String(location.speed) , accuracy: location.horizontalAccuracy, distance: String(distance!))
                }
            }
            
        }
        /*  if let location = locations.last {
         
         print("New speed is \(location.speed)")
         print("New latitude is \(location.coordinate.latitude)")
         print("New long is \(location.coordinate.longitude)")
         getAddressFromLatLon(pdblLatitude: String(location.coordinate.latitude), withLongitude: String(location.coordinate.longitude) , speed: String(location.speed) , accuracy: location.horizontalAccuracy, distance: "0.1")
         } */
        
    }
    func locationUpdateBackground(latitude : String , longitude : String , weather : String , city : String , speed : String , accuracy : Double , distance : String) {
        // print("10 sec update")
        let weatherToURl = weather.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let defaults = UserDefaults.standard
        let id = defaults.string(forKey: "id")
        let kidID = defaults.string(forKey: "idKid")
        let geoName = "KIDSMANA"
        
        let pName = defaults.string(forKey: "userName")
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        // print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        //print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        
        let name = UIDevice.current.name
        // print(name)
        
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        // print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let modifyDate1 = defaultTimeZoneStr
        let modifyDate = modifyDate1.addingPercentEncoding(withAllowedCharacters: .urlPasswordAllowed)
        print(modifyDate1)
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr1 = formatter.string(from: date as Date)
        let modifyDate2 = defaultTimeZoneStr1
        let attend = modifyDate2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Validation/postGeolocationwithdeviceinfo")! as URL)
        request.httpMethod = "POST"
        var postStrGeo : String?
        if geoFenceUpdateData.state {
            
            let kidName = defaults.string(forKey: "userNameKid")
            var notification = ""
            if geoFenceUpdateData.stauts == "EXIT"
            {
                 notification = "\(kidName!) \(geoFenceUpdateData.stauts) FROM \(geoFenceUpdateData.name)"

            }
            else
            {
                 notification = "\(kidName!) \(geoFenceUpdateData.stauts) TO \(geoFenceUpdateData.name)"

            }
            let status = geoFenceUpdateData.stauts
            postStrGeo = "empid=\(kidID!)&subscriber_id=\(id!)&geoname=\(geoName)&latitude=\(latitude)&longitude=\(longitude)&speed=\(speed)&distance=0.1&place=\(city)&status=\(status)&attedate=\(attend!)&createby=\(pName!)&modifyby=\(pName!)&modifydate=\(modifyDate!)&notification=\(notification)&accuracy=\(accuracy)&calories=0&battery_type=\(batteryType)&battery_percentage=\(Int(level*100))&network_type=\(getConnectionType())&network_operator=\(carrierName!)&device_model=\(name)&weatherinfojson=\(weatherToURl!)"
        }
        else
        {
            postStrGeo = "empid=\(kidID!)&subscriber_id=\(id!)&geoname=\(geoName)&latitude=\(latitude)&longitude=\(longitude)&speed=\(speed)&distance=0.1&place=\(city)&status=&attedate=\(attend!)&createby=\(pName!)&modifyby=\(pName!)&modifydate=\(modifyDate!)&notification=&accuracy=\(accuracy)&calories=0&battery_type=\(batteryType)&battery_percentage=\(Int(level*100))&network_type=\(getConnectionType())&network_operator=\(carrierName!)&device_model=\(name)&weatherinfojson=\(weatherToURl!)"
        }
        //let postString = "empid=\(kidID!)&subscriber_id=\(id!)&geoname=\(geoName)&latitude=\(latitude)&longitude=\(longitude)&speed=\(speed)&distance=0.1&place=\(city)&status=&attedate=\(attend!)&createby=\(pName!)&modifyby=\(pName!)&modifydate=\(modifyDate!)&notification=&accuracy=\(accuracy)&calories=0&battery_type=\(batteryType)&battery_percentage=\(Int(level*100))&network_type=\(getConnectionType())&network_operator=\(carrierName!)&device_model=\(name)&weatherinfojson=\(weatherToURl!)"
        let postString = postStrGeo!
         print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(UpdateWeather.self, from: data!)
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                
                if loginBaseResponse != nil
                {
                    let code_str = loginBaseResponse!.code
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            defaults.setValue(date, forKeyPath: "test")
                            geoFenceUpdateData.state = false
                            print("\(loginBaseResponse?.response ?? "no res")  - \(loginBaseResponse?.kidDetails?.latitude ?? "no lat") - \(loginBaseResponse?.kidDetails?.longitude ?? "no long")")
                            if let geo = loginBaseResponse?.geofences
                            {
                                if geoFenceItems?.count != geo.count
                                {
                                    geoFenceItems?.removeAll()
                                    print(geo.count)
                                    geoFenceItems = geo
                                    if let geoItems = geoFenceItems
                                    {
                                        if geoItems.count > 0
                                        {
                                            for n in 0...geoItems.count - 1 {
                                                print(geoItems[n].latitude , geoItems[n].geoname)
                                                let geofenceRegionCenter = CLLocationCoordinate2DMake(Double(geoItems[n].latitude)!,Double(geoItems[n].longitude)!)
                                                
                                                let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter,
                                                                                      radius: Double(geoItems[n].radius)!,
                                                                                      identifier: geoItems[n].geoname)
                                                geofenceRegion.notifyOnExit = true
                                                geofenceRegion.notifyOnEntry = true
                                                locationManager?.startMonitoring(for: geofenceRegion)
                                            }
                                            print("Geofence added")

                                        }
                                    }
                                }
                                else
                                {
                                    print("Geofence not added")

                                }
                            }
                        }else if code_str == 201  {
                            print("fail")
                        }
                        
                    }
                }
                else
                {
                    print("response nil location")
                }
                
            }
            
        }
        task.resume()
    }
    
    
    
    func getWeather(city : String , lat : String , long : String , speed : String , accuracy : Double , distance : String) {
        let weatherUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=62f6de3f7c0803216a3a13bbe4ea9914"
        let urlString = "\(weatherUrl)&q=\(city)"
        //print(urlString)
        let s = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        if let url = URL(string: s!){
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { [self] (data, response, error) in
                if error != nil {
                    return
                }
                if let safedata = data{
                    let stringdata = String(data: safedata, encoding: .utf8)
                    //print(stringdata)
                    // print(safedata)
                    locationUpdateBackground(latitude: lat, longitude: long, weather: stringdata! , city: city, speed: speed, accuracy: accuracy, distance: distance
                    )
                }
                else
                {
                    locationUpdateBackground(latitude: lat, longitude: long, weather: "" , city: city, speed: speed, accuracy: accuracy, distance: distance)
                }
            }
            task.resume()
        }
        else
        {
            print("eror")
        }
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String , speed : String , accuracy : Double , distance : String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        
                                        if placemarks != nil
                                        {
                                            let pm = placemarks! as [CLPlacemark]
                                            
                                            if pm.count > 0 {
                                                let pm = placemarks![0]
                                                var addressString : String = ""
                                                
                                                if pm.locality != nil {
                                                    addressString = addressString + pm.locality! + ", "
                                                }
                                                
                                                self.getWeather(city: pm.locality! , lat: pdblLatitude , long: pdblLongitude , speed: speed, accuracy: accuracy, distance: distance)
                                                
                                                //print(addressString)
                                            }
                                        }
                                    })
        
    }
}

struct geoFenceUpdate {
    var name = ""
    var stauts = ""
    var state = false
}
