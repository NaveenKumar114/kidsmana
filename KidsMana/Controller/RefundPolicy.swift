//
//  RefundPolicy.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-29.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class RefundPolicy: UIViewController {
    @IBOutlet weak var imageViewForShadow: UIImageView!
    
    @IBOutlet weak var miniDetail: UILabel!
    @IBOutlet weak var innerHeading: UILabel!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentTableView: UITableView!
    var type : String?
    @IBOutlet weak var rotateImageView: UIImageView!
    var tableViewData = [
        [refundData.generalTerms],
        [refundData.refundConditions],
        [refundData.refundProcedure],
        [refundData.chargeBackRelated],
      
    ]
   var titleForSection = ["1. General Terms" , "2. Refund Conditions" , "3. Refund Procedure" ,
                           "4. Chargeback related"]
    var hiddenSections = Set<Int>()
    override func viewDidLoad() {
        super.viewDidLoad()
        switch type {
        case "tos":
            heading.text = "Shipping/Fulfillment Policy"
            innerHeading.text = "Shipping/Fulfillment Policy"
            tableViewData = [
                [tos.tou],
                [tos.def],
                [tos.info],
                [tos.notifi],
                [tos.rules],
                [tos.dis],
                [tos.inde],
                [tos.chnages],
                [tos.sub],
                [tos.susp],
                [tos.law]
            ]
            miniDetail .text = tos.mini
            titleForSection = ["1. Terms Of Use" , "2. Definitions" , "3. Information About Mobile Application & Services" , "4. Notifications" , "5. Rules For The Usage" , "6. Disclaimer And Exclusion Of Liability" , "7. Indemnity" , "8. Changes" , "9. Subscriptions",
                "10. Suspension & Termination" , "11. Applicable Law & Jurisdiction"
            ]
        case "refund":
            heading.text = "Refund Policy"
            innerHeading.text = "Refund Policy"
            
        case "privacy":
            heading.text = "Privacy Policy"
            innerHeading.text = "Privacy Policy"
            tableViewData = [
                [privacyPolicy.introduction],
                [privacyPolicy.infoWeProcess],
                [privacyPolicy.purpose],
                [privacyPolicy.long],
                [privacyPolicy.chnages],
                [privacyPolicy.eu]
            ]
            miniDetail.text = privacyPolicy.miniDetail
            titleForSection = ["1. Introduction" , "2. Information We Process" , "3. The Purpose Of Processing Your Data" , "4. How Long We Store Your Information" , "5.Privacy Policy Changes" , "6. EU Consent Policy and Personalized Experience"]
        default:
            print("error")
        }
        imageViewForShadow.dropShadow()
        rotateImageView.rotate360Degrees()
        contentTableView.dataSource = self
        contentTableView.delegate = self
        contentTableView.register(UINib(nibName: "RefundPoilicyCell", bundle: nil), forCellReuseIdentifier: "RefundPoilicyCell")
        let headerNib = UINib.init(nibName: "RefundPolicyView", bundle: Bundle.main)
        contentTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "RefundPolicyView")
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.contentTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        self.contentTableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.contentTableView.removeObserver(self, forKeyPath: "contentSize")
    }
  
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"
        {
          if let newValue = change?[.newKey]
            {
                let newSize  = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
            
        }
    }
}

extension RefundPolicy : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let cell2 = contentTableView.dequeueReusableCell(withIdentifier: "RefundPoilicyCell") as! RefundPoilicyCell
        cell2.dataLabel.text = self.tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.text = self.tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.numberOfLines = 60
        
        return cell2
    }
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return CGFloat.init(5)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 1
        if self.hiddenSections.contains(section) {
            return 0
        }
        
        // 2
        return self.tableViewData[section].count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // 1
        let sectionButton = UIButton()
        
        // 2
        sectionButton.setTitle(titleForSection[section],
                               for: .normal)
        
        // 3
        sectionButton.titleLabel?.font =  UIFont(name: "Arial-Rounded", size: 14)!

        sectionButton.setImage(#imageLiteral(resourceName: "drop-down-arrow"), for: .normal)
        sectionButton.imageView?.tintColor = .white
        sectionButton.backgroundColor = #colorLiteral(red: 0.3901929855, green: 0.2131544948, blue: 0.6878982782, alpha: 1)
        sectionButton.layer.cornerRadius = 5
        sectionButton.contentHorizontalAlignment = .left
    //sectionButton.semanticContentAttribute = .forceRightToLeft
        // 4
        let left = tableView.frame.width - 25
        sectionButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: left, bottom: 10, right: 10);

        sectionButton.tag = section
        
        // 5
        sectionButton.addTarget(self,
                                action: #selector(self.hideSection(sender:)),
                                for: .touchUpInside)
        return sectionButton
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    @objc
    private func hideSection(sender: UIButton) {
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
            for row in 0..<self.tableViewData[section].count {
                indexPaths.append(IndexPath(row: row,
                                            section: section))
            }
            
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.contentTableView.insertRows(at: indexPathsForSection(),
                                      with: .fade)
        } else {
            self.hiddenSections.insert(section)
            self.contentTableView.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
        }
    }
   
}

