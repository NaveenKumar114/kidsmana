//
//  AddGeoFence.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-25.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import MapKit
class AddGeoFence: UIViewController {
    @IBOutlet weak var radiusLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    var annotionSet = 0
    let locationManager = CLLocationManager()
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var chooseLocationView: UIView!
    var coordinate: CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
        radiusLabel.keyboardType = .numberPad
        mapView.delegate = self
        if coordinate != nil {
            addAnnotion()
        }
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(addGeoFence))
        self.chooseLocationView.addGestureRecognizer(gesture)
        }
    

    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "geofence")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @objc func addGeoFence()
    {
        let storyboard = UIStoryboard(name: "GeoFence", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "addGeoFenceMap")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    func makePostCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/geofence")! as URL)
        request.httpMethod = "POST"
        let name = nameLabel.text!
        let latitude = coordinate?.latitude
        let longitude = coordinate?.longitude
        let latStr = String(latitude!)
        let longStr = String(longitude!)
        let locationRadius = radiusLabel.text!
        let userName = UserDefaults.standard.string(forKey: "userName")!
        let postString = "subscriber_id=\(id)&geoname=\(name)&latitude=\(latStr)&longitude=\(longStr)&radius=\(locationRadius)&status=ACTIVE&createby=\(userName)"
      
       

        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
               
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddGeoFenceDetails.self, from: data!)
                
                if loginBaseResponse != nil
                {
                    // print(String(data: data!, encoding: String.Encoding.utf8))
                    
                    
                    DispatchQueue.main.async { [self] in
                        
                        let alert = UIAlertController(title: localizedStrings.addGeoFence, message: "Success", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
                else
                {
                    print("error")
                }
                
                
            }
//                catch {
//                    self.removeSpinner()
//                    print("Error -> \(error)")
//                    showAlert(title: "Server Error", message: "Try Again later")
//                }
        }
       task.resume()
    }

    @IBAction func addGeoFenceButtonPressed(_ sender: Any) {

        if coordinate != nil
        {
            if (radiusLabel.text != "") && nameLabel.text != ""
            {
                makePostCall()
            }
            else
            {
                let alert = UIAlertController(title: localizedStrings.addGeoFence, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)

                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                        // show the alert
                        self.present(alert, animated: true, completion: nil)
            }
            //makePostCall()
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.addGeoFence, message: localizedStrings.chooseLocation, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
    }
}
extension AddGeoFence : MKMapViewDelegate,  CLLocationManagerDelegate
{
    func addAnnotion()
    {
        if annotionSet == 1 {
            mapView.removeAnnotation(pinAnnotationView.annotation!)
            
        }
        let location = coordinate!
        pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "location_pin_64"
        pointAnnotation.coordinate = location
       // pointAnnotation.title = "Palani"
        pointAnnotation.subtitle = "Current Location"
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        mapView.addAnnotation(pinAnnotationView.annotation!)
        mapView.centerCoordinate = location
        mapView.showAnnotations(self.mapView.annotations, animated: true)

        annotionSet = 1
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }

        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)

        return annotationView
    }
}
