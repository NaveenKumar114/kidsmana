//
//  viewKids.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-18.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class ViewKids: UIViewController {
    
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    var kidsData : [Kid] = []
    var weatherData : [weatherJsonToData] = []
    @IBOutlet weak var kidsListTabelView: UITableView!
    @objc func updateParent()
    {
        let storyboard = UIStoryboard(name: "UpdateParent", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "updateParent")
     (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let gestureParent = UITapGestureRecognizer(target: self, action:  #selector(updateParent))
        profilePicture.addGestureRecognizer(gestureParent)
        profilePicture.isUserInteractionEnabled = true
        kidsListTabelView.delegate = self
        kidsListTabelView.dataSource = self
        kidsListTabelView.register(UINib(nibName: "KidsListCell", bundle: nil), forCellReuseIdentifier: "KidsListCell")
        let imageUrl = imageLink.profilePicLink
        let url = URL(string: imageUrl)
       if let data = try? Data(contentsOf: url!) {//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        profilePicture.image = UIImage(data: data)
        profilePicture.contentMode = .scaleToFill
        profilePicture.makeRounded(color: UIColor.white.cgColor , borderwidth: 2)
        }
        makeGetCall()
        let defaults = UserDefaults.standard
        let imgStr = defaults.string(forKey: "img")
        if imgStr != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(imgStr!)")
            print(urlStr)
            let url = URL(string: urlStr)
            if  let data = try? Data(contentsOf: url!){
                let img = UIImage(data: data)
                profilePicture.image = img
            }
        }
        profileName.text = UserDefaults.standard.string(forKey: "userName")
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: Notification.Name("reload"), object: nil)
        
    }
    @objc func reload()
    {
        kidsData.removeAll()
        weatherData.removeAll()
        makeGetCall()
    }
    @IBAction func settingsButton(_ sender: Any) {
       //kidToSettings
        performSegue(withIdentifier: "kidToSettings", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! Settings
        destinationVC.kidsData = kidsData
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/getKids/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let kidsList = try? decoder.decode(GetKidsResponse.self, from: data!)
                
                // print(String(data: data!, encoding: String.Encoding.utf8))
                
                let code_str = kidsList?.code
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        print("success")
                        let count = kidsList?.kids?.count
                        for n in 0...count!-1{
                            let k = kidsList?.kids![n]
                            if k?.weatherinfo != nil
                            {
                                let data = k!.weatherinfo!.data(using: .utf8)!
                                let x = try? decoder.decode(weatherJsonToData.self, from: data)
                                if x != nil
                                {
                                    self.weatherData.append(x!)
                                }
                                
                            }
                            self.kidsData.append(k!)
                        }
                        
                        self.kidsListTabelView.reloadData()
                    }else if code_str == 201  {
                        
                       print("no")
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
    func deleteKid(id : String)
    {
        print(id)
        
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/deleteKid/\(id)")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let kidsList = try? decoder.decode(DeleteKid.self, from: data!)
                
                print(String(data: data!, encoding: String.Encoding.utf8)!)
                
                let code_str = kidsList?.code
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                        print("success")
                        
                    }else  {
                        let alert = UIAlertController(title: localizedStrings.kidsList, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
}

extension ViewKids : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = kidsListTabelView.dequeueReusableCell(withIdentifier: "KidsListCell") as! KidsListCell
        cell.elevate(elevation: 5.0)
        
       
        let kid = kidsData[indexPath.section]
        
        cell.nameLabel.text = kid.name
        cell.deviceName.text = kid.deviceModel
        cell.carrierName.text = kid.networkOperator
        cell.chargingLabel.text = kid.batteryType
        cell.battery.text = kid.batteryPercentage
        if weatherData.count > indexPath.section
        {
            print(weatherData.count)
            print(indexPath.section)
            let weathe = weatherData[indexPath.section]
            let temp = weathe.main?.temp
            if temp != nil
            {
                cell.weatherr.text = String(temp!)
            }
            
        }
        let imgName = kid.profile
        if imgName != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(imgName!)")
            print(urlStr)
            let url = URL(string: urlStr)
            if  let data = try? Data(contentsOf: url!){
                let img = UIImage(data: data)
                cell.profileImageView.image = img
                cell.profileImageView.contentMode = .scaleAspectFit
               //cell.profileImageView.cropAsCircleWithBorder(borderColor: .purple, strokeWidth: 2)
                //cell.profileImageView.elevateAndRounder(elevation: 5.0, color: #colorLiteral(red: 0.4926125407, green: 0.3203738332, blue: 0.6401968598, alpha: 1))
                cell.profileImageView.elevate(elevation: 5.0)
                cell.profileImageView.clipsToBounds = true
                cell.profileImageView.contentMode = .scaleToFill
                cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.height / 2
                cell.profileImageView.layer.borderWidth = 2
                cell.profileImageView.layer.borderColor = UIColor.purple.cgColor
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView() //For space between cells
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return kidsData.count
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            completionHandler(true)
            let refreshAlert = UIAlertController(title: localizedStrings.kidsList, message: localizedStrings.doYouWantToDelete, preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: localizedStrings.ok, style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                self.deleteKid(id: self.kidsData[indexPath.section].id!)
            }))

            refreshAlert.addAction(UIAlertAction(title: localizedStrings.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
            }))

            self.present(refreshAlert, animated: true, completion: nil)
        }
        
        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
            print("index path of edit: \(indexPath)")
            let storyboard = UIStoryboard(name: "EditKid", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "editKid")  as! EditKid
            mainTabBarController.kidData = self.kidsData[indexPath.section]
           
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
            completionHandler(true)
        }
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete , rename])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
}
extension UIView
{
    func elevate(elevation: Double) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = CGFloat(elevation)
        self.layer.shadowOpacity = 0.24
    }
}



extension UIView
{
    func elevateAndRounder(elevation: Double , color: CGColor)
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = CGFloat(elevation)
        self.layer.shadowOpacity = 0.24
        self.layer.borderWidth = 1
        self.layer.borderColor = color
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
