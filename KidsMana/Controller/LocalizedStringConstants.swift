//
//  NslocalizedStringConstants.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-11-09.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import Foundation

struct localizedStrings
{
    static let email = NSLocalizedString("Email", comment: "Email")
    static let parentLogin = NSLocalizedString("Parents Login", comment: "Parents login login page")
    static let kidsLogin = NSLocalizedString("Kids Login", comment: "Kids Login")
    static let login = NSLocalizedString("Login", comment: "Login")
    static let ok = NSLocalizedString("Ok", comment: "ok")
    static let scanParentsID = NSLocalizedString("Parents Setup !!! - Scan Your Parents QR code.", comment: "scan parents id error login")
    
    static let invalidEmailPassword = NSLocalizedString("Login Failed !!! - Invalid Username and Password", comment: "login failed invalid email password error")

    static let enterPassword = NSLocalizedString("Enter Password", comment: " Enter Password error")

    static let enterEmailId = NSLocalizedString("Enter Email ID", comment: "Enter email id error")
    
    static let mobileNo = NSLocalizedString("Mobile No", comment: "Mobile no")
    
    static let kids = NSLocalizedString("Kids", comment: "Kids")
    static let kidListNotFound = NSLocalizedString("Kids List Not Found", comment: "Kids List Not Found")
    static let dashboard = NSLocalizedString("Dashboard", comment: "Dashboard")
    static let cancel = NSLocalizedString("Cancel", comment: "cancel")

    static let kidListNotAvailable = NSLocalizedString("Kids List Not Found", comment: "Kids List Not Found")
    
    static let appTimeLimit = NSLocalizedString("Apps Time Limit", comment: "Apps Time Limit")
    
    static let failed = NSLocalizedString("Failed", comment: "Failed")
    static let data = NSLocalizedString("Data", comment: "Data")
    static let time = NSLocalizedString("Time", comment: "Time")
    static let Hr = NSLocalizedString("Hr", comment: "Hr")
    static let kidsList = NSLocalizedString("Kids List", comment: "kidslist")
    static let doYouWantToDelete = NSLocalizedString("Do You Want To Delete?", comment: "Do You Want To Delete?")
    static let addNewKids = NSLocalizedString("Add New Kids", comment: "Add New Kids")
    static let enterName = NSLocalizedString("Enter Name", comment: "Enter Name")
    static let enterMobile = NSLocalizedString("Enter Mobile", comment: "Enter Mobile")
    static let kidAddedSuccessfully = NSLocalizedString("Kid Added Successfully", comment: "Kid Added Successfully")
    static let chooseLocation = NSLocalizedString("Choose Location", comment: "Choose Location")
    static let locationNotFound = NSLocalizedString("Location Not Found", comment: "Location Not Found")
    static let addGeoFence = NSLocalizedString("Add Geofence", comment: "Add Geofence")
    static let kidsLocation = NSLocalizedString("Kids Locations", comment: "Kids Locations")
    static let geoFence = NSLocalizedString("Geofence", comment: "Geofence")
    static let profile = NSLocalizedString("Profile", comment: "Profile")
    static let password = NSLocalizedString("Password", comment: "Password")
    
    static let scanFailed = NSLocalizedString("Scan Failed !!!", comment: "Scan Failed !!!")

    static let parentID = NSLocalizedString("Parents ID", comment: "Parents ID")
    
    static let forgot = NSLocalizedString("Forgot", comment: "Forgot")
    static let invalidKCode = NSLocalizedString("Invalid K-Code", comment: "Invalid K-Code")
    static let home = NSLocalizedString("Home", comment: "Home")
    static let language = NSLocalizedString("Language", comment: "Language")
    static let liveTracking = NSLocalizedString("Live Tracking", comment: "Live Tracking")

    static let addKid = NSLocalizedString("Add Kid", comment: "Add Kid")
    static let logOut = NSLocalizedString("Logout", comment: "Logout")
    
    static let updateProfile = NSLocalizedString("Update Profile", comment: "Update Profile")
    static let register = NSLocalizedString("Register", comment: "Register")

}
