//
//  ViewController.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-22.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        /*let titleLabel = UILabel()
                  titleLabel.text = "Home"
        titleLabel.minimumScaleFactor = 0.2

                  titleLabel.translatesAutoresizingMaskIntoConstraints = false

                  let targetView = self.navigationController?.navigationBar
                  targetView?.addSubview(titleLabel)
        titleLabel.anchor(top: nil, left: nil, bottom: targetView?.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0, width: 222, height: 40)

                  titleLabel.centerXAnchor.constraint(equalTo: (targetView?.centerXAnchor)!).isActive = true*/
        navigationController?.navigationItem.title = "dankank"
    }
    

   
}

extension UIView{
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,  paddingTop: CGFloat, paddingLeft: CGFloat, paddingRight: CGFloat, paddingBottom: CGFloat, width: CGFloat, height: CGFloat){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top{
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let left = left{
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right{
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0{
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0{
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
    }
}
