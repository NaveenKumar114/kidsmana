//
//  EditKid.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-09.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import CountryPickerView
import Alamofire
class EditKid: UIViewController , CountryPickerViewDelegate, CountryPickerViewDataSource, UITextFieldDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
    }
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var logoCircle: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var mobileNoField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    var profileImage : UIImage?
    var countryCode : String?
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           self.view.endEditing(true)
           return false
       }
    @objc func selectImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    var kidData : Kid?
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        logo.isUserInteractionEnabled = true
        logo.addGestureRecognizer(gesture)
        passwordField.delegate = self
        mobileNoField.keyboardType = .numberPad
        mobileNoField.delegate = self
        nameField.delegate = self
        passwordField.isSecureTextEntry = true
        logoCircle.rotate360Degrees()
        logo.dropShadow(scale: true)
        
        //nameField.dropShadow()
        nameField.addShadowToTextField(cornerRadius: 10)
        passwordField.addShadowToTextField(cornerRadius: 10)
        mobileNoField.addShadowToTextField(cornerRadius: 10)
         let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        
           //cpv.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        //   mobileNo.addShadowToTextField(cornerRadius: 3)
          // passwordTextField.addShadowToTextField(cornerRadius: 3)
           cpv.delegate = self
           cpv.dataSource = self
           cpv.font = UIFont(name: "Arial-Rounded", size: 12)!
           cpv.textColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
      saveButton.layer.cornerRadius = 5
           mobileNoField.leftView = cpv
          // mobileNo.leftView = paddingView
           mobileNoField.leftViewMode = .always
        mobileNoField.isEnabled = false
        passwordField.isEnabled = false
        if let data = kidData
        {
            nameField.text = data.name
            passwordField.text = data.password
            mobileNoField.text = data.mobile
            //print(data.countryCode)
            countryCode = data.countryCode
            cpv.setCountryByPhoneCode(data.countryCode!)
            let imgStr = data.profile
            if imgStr != nil
            {
                let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(imgStr!)")
                let url = URL(string: urlStr)
                if  let data = try? Data(contentsOf: url!){
                    let img = UIImage(data: data)
                    logo.image = img
                    logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
                }
            }
        }
    }
    @IBAction func saveButtonPressed(_ sender: Any) {

        if nameField.text == ""
        {
            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        if passwordField.text == "" {
            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterPassword, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        if mobileNoField.text == "" || mobileNoField.text!.count != 10 {
            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterMobile, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if nameField.text != kidData?.name ||  profileImage != nil
            {
                if profileImage != nil
                {
                    let pass = kidData?.password
                    let mob = kidData?.mobile
                    let nam = nameField.text!
                    let kidId = kidData?.id
                       let id = UserDefaults.standard.string(forKey: "id")!
                       let parentName = UserDefaults.standard.string(forKey: "userName")!
                      // let postString = "id=\(kidId!)&subscriber_id=\(id)&parent_id=\(id)&name=\(nam)&ageStr=\(nam)&dobStr=\(nam)&education=\(nam)&mobile=\(mob!)&password=\(pass!)&cerateby=\(parentName)&modifyby=\(parentName)&country_code=\(countryCode!)&upload_type=\(2)"
                    let content = countryCode!.replacingOccurrences(of: "+", with: "%2B")
                    let parameter : Parameters = ["id" : kidId! , "subscriber_id" : id , "parent_id" : id , "name" : nam , "ageStr" : nam , "dobStr" : nam , "education" : nam , "mobile" : mob! , "password" : pass! , "cerateby" : parentName , "modifyby" : parentName , "country_code" : countryCode! , "upload_type" : 1]
                    let imageData = profileImage!.jpegData(compressionQuality: 0.50)

                    uploadImage(image: imageData!, to: URL(string: "\(baseUrl.baseUrl)api/Kidapi/add_update_kid/")!, params: parameter)
                   
                }
                else
                {
                    makePostCall()

                }
            }
            else
            {
                let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)

                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                        // show the alert
                        self.present(alert, animated: true, completion: nil)
            }
          //
        }
        
    }

    @IBAction func backButonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
    }
    func makePostCall() {
           let decoder = JSONDecoder()
           let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/add_update_kid/")! as URL)
           request.httpMethod = "POST"
        let pass = kidData?.password
        let mob = kidData?.mobile
        let nam = nameField.text!
        let kidId = kidData?.id
           let id = UserDefaults.standard.string(forKey: "id")!
           let parentName = UserDefaults.standard.string(forKey: "userName")!
           let postString = "id=\(kidId!)&subscriber_id=\(id)&parent_id=\(id)&name=\(nam)&ageStr=\(nam)&dobStr=\(nam)&education=\(nam)&mobile=\(mob!)&password=\(pass!)&cerateby=\(parentName)&modifyby=\(parentName)&country_code=\(countryCode!)&upload_type=\(2)"
       let content = postString.replacingOccurrences(of: "+", with: "%2B")
           //print(postString)
            print(content)
        let data = content.data(using: String.Encoding.utf8)
        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
           request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
           request.httpBody = content.data(using: String.Encoding.utf8)
           
           let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
               guard error == nil && data != nil else {                                                          // check for fundamental networking error
                   print("error=\(String(describing: error))")
                  
                   return
               }
               do {
                   
                   let loginBaseResponse = try? decoder.decode(UpdateKid.self, from: data!)
               
                 
                   let code_str = loginBaseResponse?.code
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                   DispatchQueue.main.async {
                   
                       if code_str == 200 {
                           self.removeSpinner()
                           print("success")
                          // NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                           let alert = UIAlertController(title: "Update Kid", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)

                                   // add an action (button)
                                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                                   // show the alert
                                   self.present(alert, animated: true, completion: nil)
                           
                           
                       }
                       else
                       {
                           self.removeSpinner()
                           let alert = UIAlertController(title: "Update Kid", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)

                                   // add an action (button)
                                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                                   // show the alert
                                   self.present(alert, animated: true, completion: nil)
                       }
                   
                   }
                   
                   
               }
   //                catch {
   //                    self.removeSpinner()
   //                    print("Error -> \(error)")
   //                    showAlert(title: "Server Error", message: "Try Again later")
   //                }
           }
          task.resume()
       }
    
   
   func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
              /*  if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)

                } */
                if let data = value.removingPercentEncoding?.data(using: .utf8) {
                   // do your stuff here
                    multipart.append(data, withName: key)
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)
                }
                            }
            multipart.append(image, withName: "profile_image", fileName: "profile.png", mimeType: "image/png")
        }

        DispatchQueue.main.async
        {
        AF.upload(multipartFormData: block, to: url)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .response{
                response in
                guard let data = response.data else { return }
                do {
                    let decoder = JSONDecoder()

                    let loginBaseResponse = try? decoder.decode(UpdateKid.self, from: data)
                
                  
                    let code_str = loginBaseResponse?.code
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)

                    DispatchQueue.main.async {
                    
                        if code_str == 200 {
                            self.removeSpinner()
                            print("success")
                           // NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.kidAddedSuccessfully, preferredStyle: UIAlertController.Style.alert)

                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        else
                        {
                            self.removeSpinner()
                            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)

                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                        }
                    
                    }
                    
                    
                }
            }
        }


}
}

extension EditKid : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {
        profileImage = image
        logo.image = image
        logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)

    }
}

extension URLEncoding {
    public func queryParameters(_ parameters: [String: Any]) -> [(String, String)] {
        var components: [(String, String)] = []
        
        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += queryComponents(fromKey: key, value: value)
        }
        return components
    }
}
