//
//  BuyNow.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-28.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class BuyNow: UIViewController {

    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var rotateImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        rotateImageView.rotate360Degrees()
        buyNowButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    

    @IBAction func buyNowPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PaymentSuccess", bundle: nil)
          let loginNavController = storyboard.instantiateViewController(identifier: "paymentSuccess")

          (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SelectSubscription", bundle: nil)
          let loginNavController = storyboard.instantiateViewController(identifier: "selectSubscription")

          (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    
}
