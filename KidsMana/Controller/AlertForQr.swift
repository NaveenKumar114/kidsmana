//
//  AlertForQr.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-30.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import AVFoundation
class AlertForQr: UIViewController   {
    
    @objc func barcode ()
    {
        let defaults = UserDefaults.standard
        parentID.text = defaults.string(forKey: "qr")
        defaults.removeObject(forKey: "qr")
    }
    @IBOutlet weak var qrScanButton: UIButton!
    
    @IBOutlet weak var parentID: UITextField!
    @IBOutlet weak var parentIdButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        qrScanButton.layer.cornerRadius = 5
        parentIdButton.layer.cornerRadius = 5
        NotificationCenter.default.addObserver(self, selector: #selector(barcode), name: Notification.Name("qr"), object: nil)
    }
    
    @IBAction func parentIdPressed(_ sender: Any) {
        if parentID.text != nil
        {
            //let snippet = parentID.text
          //let range = snippet!.range(of: "kidsmana")
            //let id = snippet![range!.upperBound...]
            let id = parentID.text
            //makePostCall(id: String(id))
            makePostCall(id: id!)
        }
      
        else
        {
            let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.invalidKCode, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func qrScanPressed(_ sender: Any) {
    }
  

    func makePostCall(id : String) {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Parents/parents_setup")! as URL)
        request.httpMethod = "POST"
        let postString = "subscriber_id=\(id)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
               
                return
            }
            do {
                
                self.removeSpinner()
                let loginBaseResponse = try? decoder.decode(LoginUserDetails.self, from: data!)
                let str = String(decoding: data!, as: UTF8.self)
                print(str)
                let code_str = loginBaseResponse!.code
              
                
                DispatchQueue.main.async {
                
                    if code_str == 200 {
                       

                        print("success")
                        let defaults = UserDefaults.standard
                        defaults.setValue(loginBaseResponse?.parentDetails?.email!, forKey: "email")
                        defaults.setValue(loginBaseResponse?.parentDetails?.id, forKey: "id")
                        defaults.setValue(loginBaseResponse?.parentDetails?.profile, forKey: "img")
                        NotificationCenter.default.post(name: Notification.Name("parentId"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                        
                    }else if code_str == 201  {
                        let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.invalidKCode , preferredStyle: UIAlertController.Style.alert)

                                // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                      
                        
                    }
                
                }
                
                
            }
//                catch {
//                    self.removeSpinner()
//                    print("Error -> \(error)")
//                    showAlert(title: "Server Error", message: "Try Again later")
//                }
        }
       task.resume()
    }
}
