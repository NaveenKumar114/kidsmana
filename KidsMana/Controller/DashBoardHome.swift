//
//  DashBoard.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-16.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class DashBoardHome: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var secondKidName: UILabel!
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var fourthKidImageView: UIImageView!
    @IBOutlet weak var thirdKidIamgeView: UIImageView!
    @IBOutlet weak var secondKidImageView: UIImageView!
    @IBOutlet weak var fourthKidName: UILabel!
    @IBOutlet weak var thirdKidName: UILabel!
    @IBOutlet weak var firstKidName: UILabel!
    @IBOutlet weak var fourthKidView: UIView!
    @IBOutlet weak var firstKidView: UIView!
    @IBOutlet weak var thirdKidView: UIView!
    @IBOutlet weak var secondKidVIew: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var firstKidImageView: UIImageView!
    @IBOutlet weak var kidsListView: UIView!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var timeLimitView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var geoFenceView: UIView!
    var slides:[Slide] = []
    var kidsData : [Kid] = []
    var weatherData : [weatherJsonToData] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    @objc func segueToLocation() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "viewLocation")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        // performSegue(withIdentifier: "ToLocation", sender: nil)
    }
    @objc func segueToTimeLimit() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "timeLimit") as AppTimeLimit
        if kidsData.count > 0
        {
            for n in 0...kidsData.count - 1
            {
                mainTabBarController.kidsName.append(kidsData[n].name!)
                mainTabBarController.kidsId.append(kidsData[n].id!)
            }
        }
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
        //   maker()
        // performSegue(withIdentifier: "ToTimeLimit", sender: nil)
    }
    @objc func segueToGeoFence() {
        
        //let storyboard = UIStoryboard(name: "CircleTabBar", bundle: nil)
     //    let mainTabBarController = storyboard.instantiateViewController(identifier: "CircleTabBar")
    // (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "geofence")
    (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
   
        
    }
    @objc func segueToProfile1() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "profile") as! Profile
        if kidsData.count > 0
        {
        mainTabBarController.currentKidId = kidsData[0].id
            mainTabBarController.kidName = kidsData[0].name
            mainTabBarController.imgName = kidsData[0].profile
            mainTabBarController.lat = kidsData[0].latitude
            mainTabBarController.long = kidsData[0].longitude
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.dashboard, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        // This is to get the S\ceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
       
    }
    @objc func segueToProfile2() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "profile") as! Profile
        if kidsData.count > 1
        {
        mainTabBarController.currentKidId = kidsData[1].id
            mainTabBarController.kidName = kidsData[1].name
            mainTabBarController.imgName = kidsData[1].profile
            mainTabBarController.lat = kidsData[1].latitude
            mainTabBarController.long = kidsData[1].longitude
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.dashboard, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        
    }
    @objc func segueToProfile3() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "profile") as! Profile
        if kidsData.count > 2
        {
        mainTabBarController.currentKidId = kidsData[2].id
            mainTabBarController.kidName = kidsData[2].name
            mainTabBarController.imgName = kidsData[2].profile
            mainTabBarController.lat = kidsData[2].latitude
            mainTabBarController.long = kidsData[2].longitude
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.dashboard, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
       
    }
    @objc func segueToProfile4() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "profile") as! Profile
        if kidsData.count > 3
        {
        mainTabBarController.currentKidId = kidsData[3].id
            mainTabBarController.kidName = kidsData[3].name
            mainTabBarController.imgName = kidsData[3].profile
            mainTabBarController.lat = kidsData[3].latitude
            mainTabBarController.long = kidsData[3].longitude
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.dashboard, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
    }
    @objc func updateParent()
    {
        let storyboard = UIStoryboard(name: "UpdateParent", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "updateParent")
     (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicImageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(segueToLocation))
        self.locationView.addGestureRecognizer(gesture)
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(segueToTimeLimit))
        self.timeLimitView.addGestureRecognizer(gesture2)
        let gesture3 = UITapGestureRecognizer(target: self, action:  #selector(segueToGeoFence))
        self.geoFenceView.addGestureRecognizer(gesture3)
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scrollView.delegate = self
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        scrollView.bringSubviewToFront(pageControl)
        locationView.layer.cornerRadius = 10
        timeLimitView.layer.cornerRadius = 10
        geoFenceView.layer.cornerRadius = 10
        locationImage.tintColor = .white
        kidsListView.dropShadow(scale: true)
       // firstKidImageView.image = UIImage(named: "a")
        firstKidImageView.contentMode = .scaleToFill
       // profilePicImageView.image = UIImage(named: "a")
       // profilePicImageView.makeRounded(color: UIColor.white.cgColor, borderwidth: 2)
        profilePicImageView.contentMode = .scaleToFill
        let gestureFirst = UITapGestureRecognizer(target: self, action:  #selector(segueToProfile1))
        let gestureSecond = UITapGestureRecognizer(target: self, action:  #selector(segueToProfile2))
        let gestureThird = UITapGestureRecognizer(target: self, action:  #selector(segueToProfile3))
        let gestureFourth = UITapGestureRecognizer(target: self, action:  #selector(segueToProfile4))
        self.firstKidView.addGestureRecognizer(gestureFirst)
        self.secondKidVIew.addGestureRecognizer(gestureSecond)
        self.thirdKidView.addGestureRecognizer(gestureThird)
        self.fourthKidView.addGestureRecognizer(gestureFourth)
        makeGetCall()
        profileName.text = UserDefaults.standard.string(forKey: "userName")
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: Notification.Name("reload"), object: nil)
        let gestureParent = UITapGestureRecognizer(target: self, action:  #selector(updateParent))
        profilePicImageView.addGestureRecognizer(gestureParent)

    }
    @objc func reload()
    {
        kidsData.removeAll()
        weatherData.removeAll()
        makeGetCall()
    }
    @IBAction func optionsPressed(_ sender: Any) {
        performSegue(withIdentifier: "dashToSettings", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! Settings
        destinationVC.kidsData = kidsData
    }
    func loadData()
    {
        let lableArray = [firstKidName , secondKidName , thirdKidName , fourthKidName]
        let imageArray = [firstKidImageView , secondKidImageView , thirdKidIamgeView , fourthKidImageView]
        for n in 0...kidsData.count-1
        {
            if n == 4
            {
                break
            }
            lableArray[n]?.text = kidsData[n].name
            
        }
        for n in 0...kidsData.count-1
        {
            
                if n == 4
                {
                    break
                }
            let imgName = kidsData[n].profile
            if imgName != nil
            {
                let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(imgName!)")
                print(urlStr)
                let url = URL(string: urlStr)
                DispatchQueue.main.async {
                if  let data = try? Data(contentsOf: url!){
                    let img = UIImage(data: data)
                    imageArray[n]?.image = img
                    imageArray[n]?.contentMode = .scaleAspectFill
                    imageArray[n]?.makeRounded(color: UIColor.red.cgColor, borderwidth: 2)
                }
                }
            }
            else
            {
                imageArray[n]?.makeRounded(color: UIColor.red.cgColor, borderwidth: 2)

            }
        }
        let defaults = UserDefaults.standard
        let imgStr = defaults.string(forKey: "img")
        if imgStr != nil
        {
        let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(imgStr!)")
        print(urlStr)
           
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        profilePicImageView.image = UIImage(data: data!)
                        profilePicImageView.contentMode = .scaleAspectFill
                        profilePicImageView.makeRounded(color: UIColor.white.cgColor, borderwidth: 2)
                        //profilePicImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 2)
                    }
                }
            
        }
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/getKids/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let kidsList = try? decoder.decode(GetKidsResponse.self, from: data!)
                
                print(String(data: data!, encoding: String.Encoding.utf8))
                
                let code_str = kidsList?.code
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        print("success")
                        let count = kidsList?.kids?.count
                        for n in 0...count!-1{
                            let k = kidsList?.kids![n]
                            if k?.weatherinfo != nil
                            {
                                let data = k!.weatherinfo!.data(using: .utf8)!
                                let x = try? decoder.decode(weatherJsonToData.self, from: data)
                                if x != nil
                                {
                                    self.weatherData.append(x!)
                                }
                                
                            }
                            self.kidsData.append(k!)
                        }
                        
                        self.loadData()
                    }else if code_str == 201  {
                        print(kidsList)
                        let alert = UIAlertController(title: localizedStrings.dashboard, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        // slide1.labelTitle.text = "A real-life bear"
        slide1.kidsMana.frame = CGRect(x: 0, y: -130, width: scrollView.frame.width, height: scrollView.frame.width)
        // slide1.contentMode = .scaleAspectFill
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.kidsMana.frame = CGRect(x: 0, y: -130, width: scrollView.frame.width, height: scrollView.frame.width)
        
        //slide2.labelTitle.text = "b real-life bear"
        
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.kidsMana.frame = CGRect(x: 0, y: -130, width: scrollView.frame.width, height: scrollView.frame.width)
        
        // slide3.labelTitle.text = "c real-life bear"
        
        
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.kidsMana.frame = CGRect(x: 0, y: -130, width: scrollView.frame.width, height: scrollView.frame.width)
        
        //   slide4.labelTitle.text = "A real-life bear"
        
        
        
        let slide5:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide5.kidsMana.frame = CGRect(x: 0, y: -130, width: scrollView.frame.width, height: scrollView.frame.width)
        
        //   slide5.labelTitle.text = "A real-life bear"
        
        
        return [slide1, slide2, slide3, slide4, slide5]
    }
    func setupSlideScrollView(slides : [Slide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(slides.count), height: scrollView.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: scrollView.frame.width * CGFloat(i), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        
        /*
         * below code changes the background color of scrollView on paging the scrollview
         */
        //        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
        
        /*
         * below code scales the imageview on paging the scrollview
         */
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        print(percentOffset)
        
        
    }
}

extension UIImageView {
    
    func makeRounded(color : CGColor , borderwidth : CGFloat) {
        
        self.layer.borderWidth = borderwidth
        self.layer.masksToBounds = false
        self.layer.borderColor = color
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

