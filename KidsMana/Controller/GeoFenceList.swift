//
//  GeoFenceList.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-22.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class GeoFenceList: UIViewController {

    @IBOutlet weak var addGeoFenceView: UIView!
    @IBOutlet weak var bottomTableContainer: UIView!
    @IBOutlet weak var topMapContainer: UIView!
    @IBOutlet weak var viewContainingLocationIcon: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainingLocationIcon.clipsToBounds = true
    viewContainingLocationIcon.layer.cornerRadius = 0.5 * viewContainingLocationIcon.bounds.size.width
        // Do any additional setup after loading the view.
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(addGeoFence))
        self.addGeoFenceView.addGestureRecognizer(gesture)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @objc func addGeoFence()
    {
        let storyboard = UIStoryboard(name: "GeoFence", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "addGeoFence")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @IBAction func viewChangeButtonPressed(_ sender: Any) {
        if self.topMapContainer.alpha == 1
        {
        UIView.animate(withDuration: 0.5) {
            self.topMapContainer.alpha = 0
            self.bottomTableContainer.alpha = 1
        }
        }
            else
            {
                UIView.animate(withDuration: 0.5) {
                    self.topMapContainer.alpha = 1
                    self.bottomTableContainer.alpha = 0
            }
            }
    
    }
    
  

}
