//
//  Profile.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-21.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import MapKit
class Profile: UIViewController {
    var annotionSet = 0
    @IBOutlet weak var dataLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var appCountLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    let locationManager = CLLocationManager()
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var startLiveTrackingView: UIView!
    @IBOutlet weak var appLabel: UILabel!
    @IBOutlet weak var appView: UIView!
    @IBOutlet weak var circle: UIImageView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tabelViewAppList: UITableView!
    @IBOutlet weak var circleWithLogo: UIImageView!
    @IBOutlet weak var viewContainingMap: UIView!
    @IBOutlet weak var adminView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    var appList : [Appusagelist] = []
    var totalTime = 0
    var totalData = 0
    var chceckDate : Date?
    var hour = 0
    var minute = 0
    var second = 0
    var kidsName : [String] = []
    var kidsId : [String] = []
    var currentKidId : String?
    var kidName : String?
    var imgName : String?
    var lat : String?
    var long : String?
    @objc func segueToLocation() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "viewLocation")
        
       print("dada")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(segueToLocation))
        startLiveTrackingView.addGestureRecognizer(gesture)
        startLiveTrackingView.isUserInteractionEnabled = true
        topView.backgroundColor = #colorLiteral(red: 0.32340765, green: 0.2170483768, blue: 0.6751087904, alpha: 1)
        tabelViewAppList.delegate = self
        tabelViewAppList.dataSource = self
        tabelViewAppList.register(UINib(nibName: "AppTimeLimitCell", bundle: nil), forCellReuseIdentifier: "AppTimeLimitCell")
        adminView.dropShadow()
        adminView.layer.cornerRadius = 20
        viewContainingMap.dropShadow()
        viewContainingMap.layer.cornerRadius = 20
        mapView.delegate = self
        mapView.layer.cornerRadius = 20
        appView.dropShadow()
        appView.layer.cornerRadius = 20
        appLabel.dropShadow()
        appLabel.elevateAndRounder(elevation: 20, color: UIColor.black.cgColor)
        circle.rotate360Degrees()
        circleWithLogo.dropShadow(scale: true)
        tabelViewAppList.dropShadow()
        tabelViewAppList.layer.cornerRadius = 20
       // navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
        startLiveTrackingView.layer.cornerRadius = 10
        //navigationController?.navigationBar.prefersLargeTitles = true
       // let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        //navigationController?.navigationBar.titleTextAttributes = textAttributes
        addAnnotion()
        if currentKidId != nil
        {
            profileName.text = kidName
            makePostCall()
        }
        print(imgName)
        if imgName != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/kids/profile/\(imgName!)")
            print(urlStr)
            let url = URL(string: urlStr)

            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    profilePic.image = UIImage(data: data!)
                    profilePic.contentMode = .scaleAspectFit

                    profilePic.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 15)
                }
            }
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabelViewAppList.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        self.tabelViewAppList.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabelViewAppList.removeObserver(self, forKeyPath: "contentSize")
    }
  
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"
        {
          if let newValue = change?[.newKey]
            {
                let newSize  = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
            
        }
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
       
    }
    func makePostCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/getAllKidsApps/")! as URL)
        request.httpMethod = "POST"
        let kidId = currentKidId!
        let postString = "kid_id=\(kidId)&subscriber_id=\(id)&app_date=2020-10-01&modifydate=2020-09-30%2021%3A13%3A46"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
               
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AppDetails.self, from: data!)
                
                if loginBaseResponse != nil
                {
               // print(String(data: data!, encoding: String.Encoding.utf8))
             
                let code_str = loginBaseResponse!.code
                
                DispatchQueue.main.async { [self] in
                
                    if code_str == 200 {
                    
                        print("success")
                        for n in 0...(loginBaseResponse?.appusagelist.count)!-1
                        {
                            
                            if let app = loginBaseResponse?.appusagelist[n]
                            {
                                let usage = app.usageTime
                                let date = NSDate(timeIntervalSince1970: Double(usage)! / 1000)
                                let formatter = DateFormatter()
                                formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                                formatter.dateFormat = "HH mm ss"

                                let delimiter = " "
                                let newstr = formatter.string(from: date as Date)
                                let token = newstr.components(separatedBy: delimiter)
                                let h = Int(token[0])!
                                let m = Int(token[1])!
                                let s = Int(token[2])!
                                if second + s > 60
                                {
                                    second = (second + s) - 60
                                    minute = minute + 1
                                    
                                }
                                else
                                {
                                    second = second + s
                                }
                                if minute + m > 60
                                {
                                    minute = (minute + m) - 60
                                    hour = hour + 1
                                }
                                else
                                {
                                    minute = minute + m
                                }
                                hour = hour + h
                                self.appList.append(app)
                                let dataBytes = Int(app.mobile)
                                self.totalData = self.totalData + dataBytes!
                               
                                
                            }
                            
                        }
                        
                       // print(self.totalData)
                        
                        self.dataLabel.text = "\(String(self.totalData / 1000000)) MB"
                        self.appCountLabel.text = String(self.appList.count)
                        self.timeLabel.text = "\(hour)H \(minute)M \(second)S"
                        self.tabelViewAppList.reloadData()
                        
                    }else if code_str == 201  {
                        self.removeSpinner()

                        let alert = UIAlertController(title: localizedStrings.profile, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)

                                // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                      
                        
                    }
                
                }
                    
                }
               
                
            }
//                catch {
//                    self.removeSpinner()
//                    print("Error -> \(error)")
//                    showAlert(title: "Server Error", message: "Try Again later")
//                }
        }
       task.resume()
    }
}

extension Profile : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabelViewAppList.dequeueReusableCell(withIdentifier: "AppTimeLimitCell") as! AppTimeLimitCell
       
        
        
        cell.elevate(elevation: 5.0)
        cell.mainBackground?.layer.cornerRadius = 5
        cell.switch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        if appList.count > 0
        {
            
                let appData = appList[indexPath.section]
                cell.nameLabel.text = appData.name
                let strDate = appData.eventTime
                let date = NSDate(timeIntervalSince1970: Double(strDate)! / 1000)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
          //  print( dateFormatter.string(from: date as Date))
            cell.date.text = dateFormatter.string(from: date as Date)
            let dataBytes = Int(appData.mobile)
            let doubleByte = Double(dataBytes!) / 1000000.0
            let doubleStr = String(format: "%.2f", ceil(doubleByte*100)/100)
            cell.dataLabel.text = "\(localizedStrings.data): \(doubleStr) MB"
            let s = appData.usageTime
            let date2 = NSDate(timeIntervalSince1970: Double(s)! / 1000)
            let formatter = DateFormatter()
            formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            formatter.dateFormat = "HH mm ss"
                        let delimiter = " "
            let newstr = formatter.string(from: date2 as Date)
            let token = newstr.components(separatedBy: delimiter)
            cell.appTimeLabel.text = "\(localizedStrings.time): \(token[0])h \(token[1])m \(token[2])s"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView() //For space between cells
           headerView.backgroundColor = UIColor.clear
           return headerView
       }
    func numberOfSections(in tableView: UITableView) -> Int {
        return appList.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension Profile : MKMapViewDelegate,  CLLocationManagerDelegate
{
    func addAnnotion()
    {
        if annotionSet == 1 {
            mapView.removeAnnotation(pinAnnotationView.annotation!)
            
        }
        if lat != "" && long != "" && lat != nil && long != nil
        {
            
            let lattitude = Double(lat!)
            let lonngitude = Double(long!)
        let location = CLLocationCoordinate2D(latitude: lattitude!, longitude: lonngitude!)
        pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "online_marker"
        pointAnnotation.coordinate = location
        pointAnnotation.title = kidName!
        pointAnnotation.subtitle = "Current Location"
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        mapView.addAnnotation(pinAnnotationView.annotation!)
        mapView.centerCoordinate = location
        mapView.showAnnotations(self.mapView.annotations, animated: true)

        annotionSet = 1
        }
        else
        {
            
        }
        }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }

        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)

        return annotationView
    }
}
