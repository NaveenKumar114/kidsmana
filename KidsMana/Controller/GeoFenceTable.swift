//
//  GeoFenceTable.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-23.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class GeoFenceTable: UIViewController {

    @IBOutlet weak var locationTableView: UITableView!
    var geoFenceData : [GeoFenceDetail] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        locationTableView.dataSource = self
        locationTableView.delegate = self
        locationTableView.register(UINib(nibName: "GeoFenceCell", bundle: nil), forCellReuseIdentifier: "GeoFenceCell")
        makeGetCall()
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/geofence/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let geolist = try? decoder.decode(GeoFenceDetails.self, from: data!)
                
                // print(String(data: data!, encoding: String.Encoding.utf8))
               
                
                
                DispatchQueue.main.async {
                    
                    if geolist != nil {
                         
                        print("success")
                       
                        for n in 0...geolist!.count - 1
                        {
                            self.geoFenceData.append(geolist![n])
                        }
                        if self.geoFenceData.count > 0
                        {
                            print(self.geoFenceData.count)
                            self.locationTableView.reloadData()
                        
                        }
                    }else   {
                        
                        let alert = UIAlertController(title: localizedStrings.geoFence, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        //self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
}

extension GeoFenceTable: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = locationTableView.dequeueReusableCell(withIdentifier: "GeoFenceCell") as! GeoFenceCell
        let geoData = geoFenceData[indexPath.section]
        cell.name.text = geoData.geoname
        cell.time.text = geoData.createdate
    //   cell.elevate(elevation: 5.0)
     //  cell.layer.cornerRadius = 10
     //   cell.layer.masksToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView() //For space between cells
           headerView.backgroundColor = UIColor.clear
        headerView.layer.cornerRadius = 10
           return headerView
       }
    func numberOfSections(in tableView: UITableView) -> Int {
        return geoFenceData.count
    }
    
    
}
