//
//  GeoFenceMap.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-24.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import MapKit
class GeoFenceMap: UIViewController {
    var annotionSet = 0

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    var geoFenceData : [GeoFenceDetail] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
      
        makeGetCall()
       
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/geofence/\(id)")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let geolist = try? decoder.decode(GeoFenceDetails.self, from: data!)
                
                // print(String(data: data!, encoding: String.Encoding.utf8))
               
                
                
                DispatchQueue.main.async {
                    
                    if geolist != nil {
                         
                        print("success")
                       
                        for n in 0...geolist!.count - 1
                        {
                            self.geoFenceData.append(geolist![n])
                        }
                        if self.geoFenceData.count > 0
                        {
                            print(self.geoFenceData.count)
                            self.addAnnotion()
                        
                        }
                    }else   {
                        
                        let alert = UIAlertController(title: localizedStrings.geoFence, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                       // self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension GeoFenceMap : MKMapViewDelegate,  CLLocationManagerDelegate
{
    
    func addAnnotion()
    {
        for n in 0...geoFenceData.count - 1
        {
            let lat = Double(geoFenceData[n].latitude)!
            let long = Double(geoFenceData[n].longitude)!
            let name = geoFenceData[n].geoname
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "location_pin_64"
        pointAnnotation.coordinate = location
        pointAnnotation.title = name
       // pointAnnotation.subtitle = "Current Location"
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        mapView.addAnnotation(pinAnnotationView.annotation!)
        mapView.centerCoordinate = location
        mapView.showAnnotations(self.mapView.annotations, animated: true)
        }
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }

        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)

        return annotationView
    }
}
