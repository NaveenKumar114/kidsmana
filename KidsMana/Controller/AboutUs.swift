//
//  AboutUs.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-29.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class AboutUs: UIViewController {

    @IBOutlet weak var circleForRtating: UIImageView!
    @IBOutlet weak var logoForShadow: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        logoForShadow.dropShadow()
        circleForRtating.rotate360Degrees()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonPresed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
  

}
