//
//  AddGeoFenceMap.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-25.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import MapKit
class AddGeoFenceMap: UIViewController, UISearchBarDelegate, MKMapViewDelegate {
    var annotionSet = 0
    let locationManager = CLLocationManager()
    var pointAnnotation:CustomPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var chooseGeofenceButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backbutton: UIButton!
    var gesture : UILongPressGestureRecognizer!
    var annotation: MKPointAnnotation?
    var coordinateToSend: CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
         gesture = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotions))
        gesture.minimumPressDuration = 1.0
        mapView.addGestureRecognizer(gesture)
        chooseGeofenceButton.layer.cornerRadius = 5
        mapView.delegate = self
        backbutton.layer.cornerRadius = 5
        searchBar.layer.cornerRadius = 5
        searchBar.searchTextField.layer.cornerRadius = 5
        searchBar.searchTextField.layer.masksToBounds = true
        searchBar.delegate = self
        mapView.isZoomEnabled = true
    }
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "GeoFence", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "addGeoFence") as! AddGeoFence
        mainTabBarController.coordinate = coordinateToSend
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "GeoFence", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "addGeoFence") as! AddGeoFence
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
        
    @objc func addAnnotions()
    {
        let location = gesture.location(in: mapView)
           let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
           coordinateToSend = coordinate
           // Add annotation:
           let annotation = MKPointAnnotation()
           annotation.coordinate = coordinate
           addAnnotion(coordinate: coordinate)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //backbutton.isHidden = true
      
        print("first")
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if searchText == ""
        {
            self.searchBar.endEditing(true)


        }
        else
        {
         
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != nil
        {
            performSearchRequest(text: searchBar.text!)

        }
        self.searchBar.endEditing(true)

    }
}


extension AddGeoFenceMap
{
    func performSearchRequest(text : String) {
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = text
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
            guard let response = response else {
                  print("Error: \(error?.localizedDescription ?? "Unknown error").")
                  return
              }
              for item in response.mapItems {
                print(item.name ?? "No phone number.")
                print(item.placemark.coordinate)
                self.setMapFocus(centerCoordinate: item.placemark.coordinate, radiusInKm: 1)
              }
         
            search.cancel()
        }
        }
    func setMapFocus(centerCoordinate: CLLocationCoordinate2D, radiusInKm radius: CLLocationDistance)
    {
    let diameter = radius * 500
        let region: MKCoordinateRegion = MKCoordinateRegion(center: centerCoordinate, latitudinalMeters: diameter, longitudinalMeters: diameter)
    self.mapView.setRegion(region, animated: false)
    }
}

extension AddGeoFenceMap :  CLLocationManagerDelegate
{
    func addAnnotion( coordinate: CLLocationCoordinate2D)
    {
        if annotionSet == 1 {
            mapView.removeAnnotation(pinAnnotationView.annotation!)
            
        }
        let location = coordinate
        pointAnnotation = CustomPointAnnotation()
        pointAnnotation.pinCustomImageName = "location_pin_64"
        pointAnnotation.coordinate = location
       // pointAnnotation.title = "Palani"
        pointAnnotation.subtitle = "Current Location"
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
        mapView.addAnnotation(pinAnnotationView.annotation!)
        mapView.centerCoordinate = location
        mapView.showAnnotations(self.mapView.annotations, animated: true)

        annotionSet = 1
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }

        let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)

        return annotationView
    }
}
