//
//  SelectSubscription.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-26.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class SelectSubscription: UIViewController {
    
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var priceLable: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var packageNameType: UILabel!
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var basicImage: UIImageView!
    var pageNo = 0
    var planNames  = ["Basic" , "Silver" , "Gold" , "Go Premium"]
    var gifNames = ["basic_new" , "silver_new" , "gold_gif" , "premium_gif"]
    var planPrice = ["$299" , "$599" , "$999" , "$1899" ]
    var planTime = ["1 Month" , "3 Months" , "Per Year" , "Lifetime"]
    var basicImageFileName = ["basic" , "silver" , "gold" , "premium"]
    override func viewDidLoad() {
        super.viewDidLoad()
        basicImage.rotate360Degrees()
        gifImageView.loadGif(asset: "basic_new")

        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        swipeRightGesture.direction = .right
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft))
        swipeLeftGesture.direction = .left
        mainView.addGestureRecognizer(swipeLeftGesture)
        mainView.addGestureRecognizer(swipeRightGesture)
        
    }
    
    @objc func swipeLeft()
    {
        if pageNo == 3
        {
          pageNo = 0
        }
        else
        {
            pageNo += 1
        }
        packageNameType.text = planNames[pageNo]
        basicImage.image = UIImage(named: basicImageFileName[pageNo])
        gifImageView.loadGif(asset: gifNames[pageNo])
        priceLable.text = planPrice[pageNo]
        timeLable.text = planTime[pageNo]
        pageControl.currentPage = pageNo
        print("left")
    }
    
    @objc func swipeRight()
    {
        if pageNo == 0
        {
          pageNo = 3
        }
        else
        {
            pageNo -= 1
        }
        packageNameType.text = planNames[pageNo]
        basicImage.image = UIImage(named: basicImageFileName[pageNo])
        gifImageView.loadGif(asset: gifNames[pageNo])
        priceLable.text = planPrice[pageNo]
        timeLable.text = planTime[pageNo]
        pageControl.currentPage = pageNo
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @IBAction func subscribeButtonPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "BuyNow", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "buyNow")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
       
    }
}

