//
//  NewPassword.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-11-08.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class NewPassword: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var rotateImage: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var kcodeText: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    var kCode : String?
    var id : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(kCode , id)
        logo.dropShadow()
        rotateImage.rotate360Degrees()
        password.isSecureTextEntry = true
        confirmPassword.isSecureTextEntry = true
        confirmPassword.delegate = self
        password.delegate = self
        kcodeText.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    func makePostCall(passwordText : String) {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/updateSubscriberPassword")! as URL)
        request.httpMethod = "POST"
        let postString = "subscriber_id=\(id!)&password=\(passwordText)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ForgetPassword.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
                
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        print(loginBaseResponse)
                        print("success")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                          let loginNavController = storyboard.instantiateViewController(identifier: "mainNavi")

                          (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
                        
                        
                        
                    }else if code_str == 201  {
                        
                        
                        let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
    @IBAction func confirmPressed(_ sender: Any) {
        if kcodeText.text == "" || kcodeText.text?.count != 6
        {
            let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: localizedStrings.invalidKCode, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if password.text == "" || password.text != confirmPassword.text{
            let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: localizedStrings.enterPassword, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if password.text!.count < 4
        {
            let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: "Password must be minumum 4 characters", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if kcodeText.text != kCode!
        {
            let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: localizedStrings.invalidKCode, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall(passwordText: password.text!)
        }
    }
}
