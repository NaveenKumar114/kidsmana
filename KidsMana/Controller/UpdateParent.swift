//
//  UpdateParent.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-10-10.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//
import Alamofire
import UIKit
import CountryPickerView
class UpdateParent: UIViewController, CountryPickerViewDelegate, CountryPickerViewDataSource, UITextFieldDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country.phoneCode)
        countryCode = country.phoneCode
    }
    
    
    @IBOutlet weak var circle: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    var profileImage : UIImage?
    @IBOutlet weak var mobileNoText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var subscribeButton: UIButton!
    var email : String?
    var name : String?
    var mobile : String?
    var countryCode : String?
    var details : LoginUserDetails?
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        logo.isUserInteractionEnabled = true
        logo.addGestureRecognizer(gesture)
        mobileNoText.keyboardType = .numberPad
        emailText.delegate = self
        nameText.delegate = self
        mobileNoText.delegate = self
        emailText.isEnabled = false
        getUserData()
        email = UserDefaults.standard.string(forKey: "email")
        name = UserDefaults.standard.string(forKey: "userName")
        mobile = UserDefaults.standard.string(forKey: "mobile")
        mobileNoText.text = mobile!
        emailText.text = email!
        nameText.text = name!
        let imgStr = UserDefaults.standard.string(forKey: "img")
        if imgStr != nil
        {
            let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(imgStr!)")
            print(urlStr)
            let url = URL(string: urlStr)

            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    logo.image = UIImage(data: data!)
                    logo.contentMode = .scaleAspectFill

                    logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
                }
            }
        }
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        
        countryCode = UserDefaults.standard.string(forKey: "countryCode")
        cpv.delegate = self
        cpv.dataSource = self
        cpv.font = UIFont(name: "Arial-Rounded", size: 12)!
        cpv.textColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
        //   let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        mobileNoText.leftView = cpv
        cpv.setCountryByPhoneCode(countryCode!)
        logo.dropShadow()
        circle.rotate360Degrees()
        mobileNoText.leftViewMode = .always
        
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 1
        subscribeButton.layer.cornerRadius = 5
        subscribeButton.layer.borderWidth = 1
    }
    @objc func selectImage()
    {
        logo.contentMode = .scaleToFill

        logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)

        //logo.makeRounded(color: UIColor.red.cgColor, borderwidth: 20)
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: "dashboardTab")
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
        print("cancel")
    }
    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
             /*   if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                } */
                if let data = value.removingPercentEncoding?.data(using: .utf8) {
                   // do your stuff here
                    multipart.append(data, withName: key)
                    print(String(data: data, encoding: String.Encoding.utf8) as Any)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile.png", mimeType: "image/png")
        }
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
              
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(UpdateParentDetail.self, from: data)
                        let code_str = loginBaseResponse!.code
                        
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                print(String(data: data, encoding: String.Encoding.utf8) as Any)
                                let defaults = UserDefaults.standard
                                
                                print("success")
                                defaults.setValue(loginBaseResponse?.companydetails?.name! , forKey: "userName")
                                defaults.setValue(loginBaseResponse?.companydetails?.contact, forKey: "mobile")
                                defaults.setValue(loginBaseResponse?.companydetails?.countryCode, forKey: "countryCode")
                                defaults.setValue(loginBaseResponse?.companydetails?.profile, forKey: "img")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let loginNavController = storyboard.instantiateViewController(identifier: "dashboardTab")
                                
                                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
                            }else if code_str == 201  {
                                
                                let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                        }
                        
                    }
                }
        
        
        
    }
    @IBAction func subscribeButtonPressed(_ sender: Any) {
        // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //  dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        if nameText.text != "" && mobileNoText.text != "" && emailText.text != ""
        {
            if nameText.text != name || mobileNoText.text != mobile || emailText.text != email || profileImage != nil
            {
                if profileImage != nil
                {
                    name = nameText.text
                    mobile = mobileNoText.text
                    email = emailText.text
                    let companyName = details?.parentDetails?.companyName
                    
                    let modifyBy = details?.parentDetails?.modifyBy
                    
                    let id = details?.parentDetails?.id
                    
                    
                    let date = NSDate()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    formatter.timeZone = TimeZone(abbreviation: "UTC")
                    let defaultTimeZoneStr = formatter.string(from: date as Date)
                    let modifyDate = defaultTimeZoneStr
                   // let content = countryCode!.replacingOccurrences(of: "+", with: "%2B")
                    let imageData = profileImage!.jpegData(compressionQuality: 0.50)
                    let parameter : Parameters = ["upload_type" : 1 , "subscriber_id" : id! , "name" : name! , "company_name" : companyName! , "contact" : mobile! , "country_code" : countryCode! , "modify_by" : modifyBy! , "modify_date" : modifyDate]
                    uploadImage(image: imageData!, to: URL(string: "\(baseUrl.baseUrl)api/employee/update_admin_register")!, params: parameter)
                }
                else
                {
                    makePostCall()
                    
                }
            }
            else
            {
                let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.enterName, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        ///       let loginNavController = storyboard.instantiateViewController(identifier: "dashboardTab")
        
        
        //  (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    func getUserData() {
        let decoder = JSONDecoder()
        let email = UserDefaults.standard.string(forKey: "email")
        let password = UserDefaults.standard.string(forKey: "password")
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Parents/parents_login")! as URL)
        request.httpMethod = "POST"
        let postString = "email=\(email!)&password=\(password!)&tokenid=\(password!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginUserDetails.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
                
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        self.details = loginBaseResponse
                        print("success")
                        
                    }else if code_str == 201  {
                        self.removeSpinner()
                        
                        let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
    func makePostCall() {
        name = nameText.text
        mobile = mobileNoText.text
        email = emailText.text
        let companyName = details?.parentDetails?.companyName
        
        let modifyBy = details?.parentDetails?.modifyBy
        
        let id = details?.parentDetails?.id
        
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let modifyDate = defaultTimeZoneStr
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/update_admin_register")! as URL)
        request.httpMethod = "POST"
        let postString = "upload_type=2& subscriber_id=\(id!)&name=\(name!)&company_name=\(companyName!)&contact=\(mobile!)&country_code=\(countryCode!)&modify_by=\(modifyBy!)&modify_date=\(modifyDate)"
        let content = postString.replacingOccurrences(of: "+", with: "%2B")
        
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = content.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(UpdateParentDetail.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
                
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        let defaults = UserDefaults.standard
                        
                        print("success")
                        defaults.setValue(loginBaseResponse?.companydetails?.name! , forKey: "userName")
                        defaults.setValue(loginBaseResponse?.companydetails?.contact, forKey: "mobile")
                        defaults.setValue(loginBaseResponse?.companydetails?.countryCode, forKey: "countryCode")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let loginNavController = storyboard.instantiateViewController(identifier: "dashboardTab")
                        
                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
                        
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: localizedStrings.updateProfile, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                }
            }
        }
        task.resume()
    }
    
}

extension UpdateParent : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {
        profileImage = image
        logo.image = image
        logo.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
        
    }
}
