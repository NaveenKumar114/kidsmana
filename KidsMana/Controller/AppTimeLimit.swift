//
//  AppTimeLimit.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-19.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import DatePicker

class AppTimeLimit: UIViewController {
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var kidName: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var appLabel: UILabel!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var dropdown: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var tabelViewAppList: UITableView!
    var appList : [Appusagelist] = []
    var totalTime = 0
    var totalData = 0
    var chceckDate : Date?
    var hour = 0
    var minute = 0
    var second = 0
    var kidsName : [String] = []
    var kidsId : [String] = []
    var currentKidId : String?
    var timePickerHour = 0
    var timePickerSecond = 0
    var picker  = UIPickerView()
    var indexOfSelected : IndexPath!
    var pickerHour = 0
    var pickerMinute = 0
    var day = "0"
    var month = "0"
    var year = "0"
    @objc func showKids()
    {
        //FIXME: message 
        let alert = UIAlertController(title: localizedStrings.kids, message: nil, preferredStyle: .actionSheet)
        for n in 0...kidsName.count - 1 {
            alert.addAction(UIAlertAction(title: kidsName[n] , style: .default , handler:{ [self] (UIAlertAction)in
                print(self.kidsName[n])
                self.kidName.text = self.kidsName[n]
                currentKidId = self.kidsId[n]
                appList.removeAll()
                totalTime = 0
                totalData = 0
                hour = 0
                second = 0
                minute = 0
                
                tabelViewAppList.reloadData()
                makePostCall()
            }))
        }
        
        alert.addAction(UIAlertAction(title: localizedStrings.cancel, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        day = date.day()
        month = date.month()
        year = String(date.year())
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        dateButton.setTitle(result, for: .normal)
        appLabel.text = "0"
        dataLabel.text = "0"
        dataView.dropShadow()
        dataView.layer.cornerRadius = 10
        //   dataView.layer.borderWidth = 1
        dropdown.layer.borderWidth = 1
        dropdown.layer.borderColor = #colorLiteral(red: 0.5410231948, green: 0.3333895802, blue: 0.7410029173, alpha: 1)
        dropdown.layer.cornerRadius = 10
        tabelViewAppList.delegate = self
        tabelViewAppList.dataSource = self
        tabelViewAppList.register(UINib(nibName: "AppTimeLimitCell", bundle: nil), forCellReuseIdentifier: "AppTimeLimitCell")
        dateButton.layer.borderWidth = 1
        
        dateButton.layer.cornerRadius = 4
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(showKids))
        self.dropdown.addGestureRecognizer(gesture)
        if kidsName.count > 0
        {
            kidName.text = kidsName[0]
            currentKidId = kidsId[0]
            makePostCall()
            
        }
        else
        {
            kidName.text = localizedStrings.kidListNotAvailable
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        
    }
    
    @IBAction func dateButton(_ sender: Any) {
        let minDate = DatePickerHelper.shared.dateFrom(day: 18, month: 08, year: 1990)!
        let maxDate = DatePickerHelper.shared.dateFrom(day: 18, month: 08, year: 2030)!
        let today = Date()
        // Create picker object
        let datePicker = DatePicker()
        // Setup
        datePicker.setup(beginWith: today, min: minDate, max: maxDate) { [self] (selected, date) in
            if selected, let selectedDate = date {
                //   print(selectedDate.string())
                self.month = String(selectedDate.month())
                self.year = String(selectedDate.year())
                self.day = String(selectedDate.day())
                self.dateButton.setTitle("\(selectedDate.day()).\(selectedDate.month()).\(selectedDate.year())", for: .normal)
                makePostCall()
            } else {
                print("Cancelled")
            }
        }
        // Display
        datePicker.show(in: self, on: sender as? UIView)
    }
    func makePostCall() {
        
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")!
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/getAllKidsApps/")! as URL)
        request.httpMethod = "POST"
        let kidId = currentKidId!
        let date = "\(year)-\(month)-\(day)"
        let postString = "kid_id=\(kidId)&subscriber_id=\(id)&app_date=\(date)&modifydate="
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AppDetails.self, from: data!)
                
                if loginBaseResponse != nil
                {
                    // print(String(data: data!, encoding: String.Encoding.utf8))
                    
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            totalTime = 0
                            totalData = 0
                            hour = 0
                            second = 0
                            minute = 0
                            appLabel.text = "0"
                            appList.removeAll()
                            print("success")
                            //print(UserDefaults.standard.string(forKey: "userName"))
                            
                            for n in 0...(loginBaseResponse?.appusagelist.count)!-1
                            {
                                
                                if let app = loginBaseResponse?.appusagelist[n]
                                {
                                    let usage = app.usageTime
                                    let date = NSDate(timeIntervalSince1970: Double(usage)! / 1000)
                                    let formatter = DateFormatter()
                                    formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                                    formatter.dateFormat = "HH mm ss"
                                    
                                    let delimiter = " "
                                    let newstr = formatter.string(from: date as Date)
                                    let token = newstr.components(separatedBy: delimiter)
                                    let h = Int(token[0])!
                                    let m = Int(token[1])!
                                    let s = Int(token[2])!
                                    if second + s > 60
                                    {
                                        second = (second + s) - 60
                                        minute = minute + 1
                                        
                                    }
                                    else
                                    {
                                        second = second + s
                                    }
                                    if minute + m > 60
                                    {
                                        minute = (minute + m) - 60
                                        hour = hour + 1
                                    }
                                    else
                                    {
                                        minute = minute + m
                                    }
                                    hour = hour + h
                                    self.appList.append(app)
                                    let dataBytes = Int(app.mobile)
                                    self.totalData = self.totalData + dataBytes!
                                    
                                    
                                }
                                
                            }
                            
                            // print(self.totalData)
                            
                            self.dataLabel.text = "\(String(self.totalData / 1000000)) MB"
                            self.appLabel.text = String(self.appList.count)
                            self.timeLabel.text = "\(hour)H \(minute)M \(second)S"
                            self.tabelViewAppList.reloadData()
                            
                        }else if code_str == 201  {
                            self.removeSpinner()
                            
                            let alert = UIAlertController(title: localizedStrings.appTimeLimit, message: localizedStrings.kidListNotFound, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
    func changeAppLimit()
    {
        
    }
    
    
}
extension AppTimeLimit:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerHour = 0
        pickerMinute = 0
        switch component {
        case 0:
            return 24
        case 1:
            return 60

        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.frame.size.width/3
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return "\(row) Hour"
        case 1:
            return "\(row) Minute"
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            print("h\(row)")
            pickerHour = row
          //  hour = row
        case 1:
            pickerMinute = row
            print("m\(row)")
           
        default:
            break;
        }
    }
}
extension AppTimeLimit : AppTimeDelegate
{
    func onClickTimer(index: IndexPath) {
        indexOfSelected = index
        picker = UIPickerView.init()
            picker.delegate = self
        picker.dataSource = self
            picker.backgroundColor = UIColor.white
            picker.setValue(UIColor.black, forKey: "textColor")
            picker.autoresizingMask = .flexibleWidth
            picker.contentMode = .center
            picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
            self.view.addSubview(picker)

            toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
            toolBar.barStyle = .blackTranslucent
            toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
            self.view.addSubview(toolBar)
        
    }
    func setAppUsageTimeLimit()
    {
        let timestamp = NSDate().timeIntervalSince1970
        let timeStampMilli = timestamp*1000
        let d = String(timeStampMilli)
        let d1 = d.components(separatedBy: ".")
        let appID = appList[indexOfSelected.section].appid
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/app_usage_timelimit/")! as URL)
        request.httpMethod = "POST"
        let postString = "appid=\(appID)&app_time_limit_enable=1&app_time_limit=\(d1[0])&app_time=\(pickerHour):\(pickerMinute)"
       // print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                return
            }
            do {
                let loginBaseResponse = try? decoder.decode(AppIconHideShow.self, from: data!)
                let code_str = loginBaseResponse?.code
                //print(code_str)
                //print(postString)
               print(String(data: data!, encoding: String.Encoding.utf8)!)
                //self.makePostCall()
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        print("successfully Added")
                        let cell = tabelViewAppList.cellForRow(at: indexOfSelected) as! AppTimeLimitCell
                        cell.timeLabel.text = "\(pickerHour):\(pickerMinute)"
                        appList[indexOfSelected.section].appTimeLimit = "\(pickerHour):\(pickerMinute)"
                        appList[indexOfSelected.section].appTimeLimit = d1[0]
                        cell.timerButton.imageView?.tintColor = .green
                        tabelViewAppList.reloadData()
                    }
                    else
                    {
                        appList[indexOfSelected.section].appTime = "\(pickerHour):\(pickerMinute)"
                        appList[indexOfSelected.section].appTimeLimit = d1[0]
                        tabelViewAppList.reloadData()
                       // print(code_str)
                        print("failed")
                    }
                }
            }
        }
        task.resume()
    }
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        setAppUsageTimeLimit()
    }
    func switchChanged(index : IndexPath , state : Bool) {
        indexOfSelected = index
        var appLimit = 0
        print(appList[index.section].appiconHideshow)
        if state
        {
            appLimit = 1
        }
        else
        {
            appLimit = 0
        }
        let appID = appList[index.section].appid
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Kidapi/appicon_hideshowbykid/")! as URL)
        request.httpMethod = "POST"
        let postString = "appid=\(appID)&appicon_hideshow=\(appLimit)"
        //print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                return
            }
            do {
                let loginBaseResponse = try? decoder.decode(AppIconHideShow.self, from: data!)
                let code_str = loginBaseResponse?.code
                DispatchQueue.main.async {
                    if code_str == 200 {
                        print("successfully Added")
                        //self.makePostCall()
                    }
                    else
                    {
                        let alert = UIAlertController(title: localizedStrings.appTimeLimit, message: localizedStrings.failed, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
}




extension AppTimeLimit : UITableViewDelegate , UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabelViewAppList.dequeueReusableCell(withIdentifier: "AppTimeLimitCell") as! AppTimeLimitCell
        cell.elevate(elevation: 5.0)
        cell.mainBackground?.layer.cornerRadius = 5
        cell.switch.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        if appList.count > 0
        {
            
            let appData = appList[indexPath.section]
            cell.nameLabel.text = appData.name
            let strDate = appData.eventTime
            print(appData)
            print(strDate)
            let date = NSDate(timeIntervalSince1970: Double(strDate)! / 1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
            //  print( dateFormatter.string(from: date as Date))
            cell.date.text = dateFormatter.string(from: date as Date)
            let dataBytes = Int(appData.mobile)
            let doubleByte = Double(dataBytes!) / 1000000.0
            let doubleStr = String(format: "%.2f", ceil(doubleByte*100)/100)
            cell.dataLabel.text = "\(localizedStrings.data): \(doubleStr) MB"
            let s = appData.usageTime
            let date2 = NSDate(timeIntervalSince1970: Double(s)! / 1000)
            let formatter = DateFormatter()
            formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            formatter.dateFormat = "HH mm ss"
            let delimiter = " "
            let newstr = formatter.string(from: date2 as Date)
            let token = newstr.components(separatedBy: delimiter)
            cell.appTimeLabel.text = "\(localizedStrings.time): \(token[0])h \(token[1])m \(token[2])s"
            if appData.appiconHideshow == "1"
            {
                cell.switch.isOn = true
            }
            else
            {
                cell.switch.isOn = false
                
            }
            if appData.appTimeLimit != "0" && appData.appTime !=  "0:0"
            {
                
                cell.timeLabel.text = appData.appTime
                cell.timerButton.imageView?.tintColor = .green
            }
            else
            {
                cell.timeLabel.text = "00:00:00 \(localizedStrings.Hr)"
                cell.timerButton.imageView?.tintColor = .red

            }
            cell.delegate = self
            cell.indexPath = indexPath
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView() //For space between cells
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return appList.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("dada")
        //print(appList[indexPath.section].eventTime)
    }
    
    
}
