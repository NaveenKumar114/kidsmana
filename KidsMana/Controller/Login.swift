//
//  ViewController.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-14.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

//TODO: Country picker delegate
import UIKit
import SystemConfiguration
import CoreTelephony
import CountryPickerView
import iOSDropDown
import QuartzCore
import FirebaseMessaging
import FirebaseInstanceID
class Login: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var kidsmanaToWeb: UILabel!
    @IBOutlet weak var loginImageView: UIImageView!
    @IBOutlet weak var languageButtonView: UIView!
    @IBOutlet weak var kidsOrParentsLoginButton: UIButton!
    @IBOutlet weak var logoCircle: UIImageView!
    @IBOutlet weak var languagePicker: UIPickerView!
    var pickerData: [String] = [String]()
    
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var kidsOrParentsLoginLable: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoImageVIew: UIImageView!
    @IBOutlet weak var mobileNo: UITextField!
    var tokenId = ""

    var username_str: String = ""
    var password_str: String = ""
    var response_json: String = ""
    var client_id: String = ""
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    @objc func openAlert()
    { let storyboard = UIStoryboard(name: "AlertForQr", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "alertForQr")
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
        //  performSegue(withIdentifier: "qr", sender: nil)
        //present(alert() , animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //UserDefaults.standard.set(["ms"], forKey: "AppleLanguage")
        //UserDefaults.standard.synchronize()
        passwordTextField.delegate = self
        mobileNo.delegate = self
       /* Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            self.tokenId = token
          }
        }*/
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(openAlert))
        passwordTextField.isSecureTextEntry = true
        self.logoCircle.addGestureRecognizer(gesture2)
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        //cpv.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        //   mobileNo.addShadowToTextField(cornerRadius: 3)
        // passwordTextField.addShadowToTextField(cornerRadius: 3)
        cpv.delegate = self
        cpv.dataSource = self
        cpv.font = UIFont(name: "Arial-Rounded", size: 12)!
        cpv.textColor = #colorLiteral(red: 0.390453279, green: 0.2120705843, blue: 0.6840612292, alpha: 1)
        //   let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        mobileNo.leftView = cpv
        // mobileNo.leftView = paddingView
        mobileNo.leftViewMode = .always
        // saveImage()
        mobileNo.layer.borderColor = UIColor.black.cgColor
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        //   languageButton.layer.cornerRadius = 5
        // languageButton.layer.borderWidth = 1
        languageButtonView.layer.cornerRadius = 5
        languageButtonView.layer.borderWidth = 1
        logoCircle.rotate360Degrees()
        // logoImageVIew.applyshadowWithCorner(containerView: viewForShadow, cornerRadious: 100)
        logoImageVIew.dropShadow(scale: true)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        languageButtonView.addGestureRecognizer(gesture)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadText), name: Notification.Name("parentId"), object: nil)
        reloadText()
        let gestureToWeb = UITapGestureRecognizer(target: self, action:  #selector(self.toWeb))
        kidsmanaToWeb.addGestureRecognizer(gestureToWeb)
    }
    @objc func toWeb()
    {
        if let url = URL(string: "https://www.kidsmana.com") {
            UIApplication.shared.open(url)
        }
       

    }
   
    @objc func reloadText()
    {
       
        kidsOrParentsLoginButton.setTitle("\(localizedStrings.kidsLogin)?", for: .normal)
        kidsOrParentsLoginLable.text = localizedStrings.parentLogin
        mobileNo.placeholder = localizedStrings.email
        mobileNo.leftViewMode = .never
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "img") != nil && defaults.string(forKey: "img") != ""
        {
            
            let urlStr = ("\(baseUrl.baseUrl)uploads/parent/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    logoImageVIew.image = UIImage(data: data!)
                    
                    logoImageVIew.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
                    logoImageVIew.contentMode = .scaleAspectFit
                    
                }
            }
        }
        mobileNo.text = defaults.string(forKey: "email")
    }
    @IBAction func swichChnaged(_ sender: UISwitch) {
       
        if sender.isOn == true
        {
            passwordTextField.isSecureTextEntry = false
        }
        else
        {
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField!
    @objc func checkAction(sender : UITapGestureRecognizer) {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url)
       // performSegue(withIdentifier: "languagePicker", sender: nil)
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "signUp")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ForgotPassword", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "forgetNavigation")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.showSpinner(onView: loginButton)
        username_str = mobileNo.text!
        password_str = passwordTextField.text!
        if let id = UserDefaults.standard.string(forKey: "id")
        {
            print(id)
            if (username_str.isEmpty==true) {
                let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.enterEmailId, preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                self.removeSpinner()
                
            } else if (password_str.isEmpty==true) {
                let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.enterPassword, preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                self.removeSpinner()
                
            } else {
                if kidsOrParentsLoginLable.text == localizedStrings.kidsLogin
                {
                    let id = UserDefaults.standard.string(forKey: "id")
                    if id != nil
                    {
                        makePostCallKid()
                    }
                    else
                    {
                        let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.scanParentsID, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {Messaging.messaging().token { [self] token, error in
                    if let error = error {
                      print("Error fetching FCM registration token: \(error)")
                        makePostCall();

                    } else if let token = token {
                      print("FCM registration token: \(token)")
                      self.tokenId = token
                        makePostCall();

                    }
                  }
                    print("parents")
                }
                
                
            }
        }
        else
        {
            let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.scanParentsID, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            self.removeSpinner()
        }
        
        
        
    }
    
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    func makePostCallKid() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
        Messaging.messaging().token { token, error in
           if let error = error {
             print("Error fetching FCM registration token: \(error)")
           } else if let token = token {
             print("FCM registration token: \(token)")
             self.tokenId = token
           }
         }
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/kid_login")! as URL)
        request.httpMethod = "POST"
        let postString = "mobile=\(username_str)&password=\(password_str)&subscriber_id=\(id!)&tokenid=\(tokenId)&battery_type=\(batteryType)&battery_percentage=\(Int(level*100))&network_type=\(getConnectionType())&network_operator=\(carrierName!)&phone_type=0&device_name=\(name)&device_manufracture=Apple&device_brand=\(model)&device_os=\(version)&device_model=\(model)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(KidLogin.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
                
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        self.removeSpinner()
                        print("success")
                        let defaults = UserDefaults.standard
                        defaults.setValue("kid", forKey: "isLoggedIn")
                        
                        defaults.setValue(self.password_str , forKey: "password")
                        defaults.setValue(loginBaseResponse?.logindetails?.createdBy , forKey: "userName")
                        defaults.setValue(loginBaseResponse?.logindetails?.name , forKey: "userNameKid")
                        defaults.setValue(loginBaseResponse?.logindetails?.id, forKey: "idKid")
                        defaults.setValue(loginBaseResponse?.logindetails?.mobile, forKey: "mobileKid")
                        if loginBaseResponse?.logindetails?.profile != ""
                        {
                            defaults.setValue(loginBaseResponse?.logindetails?.profile, forKey: "imgKid")
                            
                        }
                        defaults.setValue(loginBaseResponse?.logindetails?.subscriberID, forKey: "id")
                        let storyboard = UIStoryboard(name: "KidDashboard", bundle: nil)
                        let mainTabBarController = storyboard.instantiateViewController(identifier: "KidDashboard")
                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
                    }else if code_str == 201  {
                        self.removeSpinner()
                        // print(loginBaseResponse)
                        let alert = UIAlertController(title: localizedStrings.login, message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func makePostCall() {
        
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/Parents/parents_login_ios")! as URL)
        request.httpMethod = "POST"
        
        let postString = "email=\(username_str)&password=\(password_str)&tokenid=\(tokenId)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginUserDetails.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
                //   let response_str = loginBaseResponse!.response
                
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        print(loginBaseResponse as Any)
                        print("success")
                        let defaults = UserDefaults.standard
                        defaults.setValue("true", forKey: "isLoggedIn")
                        defaults.setValue(loginBaseResponse?.parentDetails?.email! , forKey: "email")
                        defaults.setValue(loginBaseResponse?.parentDetails?.name! , forKey: "userName")
                        defaults.setValue(loginBaseResponse?.parentDetails?.id!, forKey: "id")
                        defaults.setValue(loginBaseResponse?.parentDetails?.contact!, forKey: "mobile")
                        if loginBaseResponse?.parentDetails?.profile != ""
                        {
                            defaults.setValue(loginBaseResponse?.parentDetails?.profile, forKey: "img")
                            
                        }
                        defaults.setValue(loginBaseResponse?.parentDetails?.countryCode, forKey: "countryCode")
                        defaults.setValue(password_str , forKey: "password")
                        
                        self.removeSpinner()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
                        
                    }else if code_str == 201  {
                        self.removeSpinner()
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
    
    @IBAction func kidsOrParentsLoginPressed(_ sender: Any) {
        switch kidsOrParentsLoginButton.titleLabel?.text {
        case "\(localizedStrings.kidsLogin)?":
            kidsOrParentsLoginButton.setTitle("\(localizedStrings.parentLogin)?", for: .normal)
            kidsOrParentsLoginLable.text = localizedStrings.kidsLogin
            mobileNo.placeholder = localizedStrings.mobileNo
            mobileNo.leftViewMode = .always
            mobileNo.text = ""
            loginImageView.image = UIImage(named: "002-phone-call")
            loginImageView.tintColor = #colorLiteral(red: 0.752874434, green: 0.7529839873, blue: 0.7528504729, alpha: 1)
            
            break
        case "\(localizedStrings.parentLogin)?":
            kidsOrParentsLoginButton.setTitle("\(localizedStrings.kidsLogin)?", for: .normal)
            kidsOrParentsLoginLable.text = localizedStrings.parentLogin
            mobileNo.placeholder = localizedStrings.email
            mobileNo.leftViewMode = .never
            let defaults = UserDefaults.standard
            loginImageView.image = UIImage(named: "follower")
            //loginImageView.tintColor = .black
            mobileNo.text = defaults.string(forKey: "email")
            
            break
        default:
            
            break
        }
    }
    
    
    func alert() -> AlertForQr
    {
        let storyboard = UIStoryboard(name: "AlertForQr", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "alertForQr") as! AlertForQr
        return mainTabBarController
    }
    
    
}


extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 3) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

extension Login :  CountryPickerViewDelegate, CountryPickerViewDataSource
{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print("ada")
    }
    
    
}

extension UIImageView {
    func applyshadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
    func cropAsCircleWithBorder(borderColor : UIColor, strokeWidth: CGFloat)
    {
        var radius = min(self.bounds.width, self.bounds.height)
        var drawingRect : CGRect = self.bounds
        drawingRect.size.width = radius
        drawingRect.origin.x = (self.bounds.size.width - radius) / 2
        drawingRect.size.height = radius
        drawingRect.origin.y = (self.bounds.size.height - radius) / 2
        
        radius /= 2
        
        var path = UIBezierPath(roundedRect: drawingRect.insetBy(dx: strokeWidth/2, dy: strokeWidth/2), cornerRadius: radius)
        let border = CAShapeLayer()
        border.fillColor = UIColor.clear.cgColor
        border.path = path.cgPath
        border.strokeColor = borderColor.cgColor
        border.lineWidth = strokeWidth
        self.layer.addSublayer(border)
        
        path = UIBezierPath(roundedRect: drawingRect, cornerRadius: radius)
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 10
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
extension UITextField {
    
    func addShadowToTextField(color: UIColor = UIColor.gray, cornerRadius: CGFloat) {
        
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 1.0
        self.backgroundColor = .white
        self.layer.cornerRadius = cornerRadius
    }
}

extension UITextField
{
    func roundCorners()
    {
        
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
    }
}


var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}


