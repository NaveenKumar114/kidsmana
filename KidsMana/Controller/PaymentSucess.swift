//
//  PaymentSucess.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-28.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
import SwiftGifOrigin
class PaymentSucess: UIViewController {

    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var gifImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        gifImage.loadGif(asset: "sucess_animation")
        getStartedButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    @IBAction func getStartedButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTabBarController = storyboard.instantiateViewController(identifier: "dashboardTab")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
