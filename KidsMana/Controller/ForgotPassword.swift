//
//  Forgot Password.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-26.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    var kcode : String?
    var id : String?
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var rotateImage: UIImageView!
    @IBOutlet weak var emailLable: UITextField!
    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 1
        sendEmailButton.layer.cornerRadius = 5
        sendEmailButton.layer.borderWidth = 1
        logo.dropShadow()
        rotateImage.rotate360Degrees()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: "login")
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    
    @IBAction func sendEmailBUttonPressed(_ sender: Any) {
        if emailLable.text?.isValidEmail == false
        {
            let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: "Invalid Email", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let number = arc4random_uniform(900000) + 100000;
            print(number)
            let email = emailLable.text
            kcode = String(number)
            makePostCall(email: email!, kCode: String(number))
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "confirmPassword"
        {
            if let nextViewController = segue.destination as? NewPassword {
                nextViewController.kCode = kcode!
                nextViewController.id = id!
            }
        }
    }
    func makePostCall(email: String , kCode : String) {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseUrl.baseUrl)api/employee/changePassswordRequest")! as URL)
        request.httpMethod = "POST"
        let postString = "email=\(email)&kcode=\(kCode)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ForgetPassword.self, from: data!)
                
                
                let code_str = loginBaseResponse!.code
               // let response_str = loginBaseResponse!.response
                
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        print(loginBaseResponse?.companydetails?.id!)
                        self.id = loginBaseResponse?.companydetails?.id!
                        self.kcode = kCode
                        self.performSegue(withIdentifier: "confirmPassword", sender: nil)
                        print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        
                        print(loginBaseResponse as Any)

                        let alert = UIAlertController(title: "\(localizedStrings.forgot) \(localizedStrings.password)", message: localizedStrings.invalidEmailPassword, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: localizedStrings.ok, style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
    
}
