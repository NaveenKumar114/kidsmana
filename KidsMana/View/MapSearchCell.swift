//
//  MapSearchCell.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-25.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class MapSearchCell: UITableViewCell {

    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
