//
//  KidsListCell.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-18.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class KidsListCell: UITableViewCell {

    @IBOutlet weak var chargingLabel: UILabel!
    @IBOutlet weak var battery: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherr: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var carrierName: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       // profileImageView.makeRounded(color: #colorLiteral(red: 0.2652241886, green: 0.1567378342, blue: 0.5432885289, alpha: 1))
        profileImageView.contentMode = .scaleToFill
   //  self.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
   //  self.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
   //  self.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
   //  self.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
}
