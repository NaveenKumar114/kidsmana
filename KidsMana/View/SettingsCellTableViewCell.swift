//
//  SettingsCellTableViewCell.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-17.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class SettingsCellTableViewCell: UITableViewCell {
    @IBOutlet weak var menuLabel: UILabel!
    
    @IBOutlet weak var menuImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
