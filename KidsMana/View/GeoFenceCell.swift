//
//  GeoFenceCell.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-23.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit

class GeoFenceCell: UITableViewCell {
    @IBOutlet weak var viewContainingImage: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var dummy: UIView!
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainingImage.clipsToBounds = true
        viewContainingImage.layer.cornerRadius = 26
    //    containerView.layer.cornerRadius = 10
  //  dataView.layer.cornerRadius = 8
      //  dummy.layer.cornerRadius = 8
        containerView.layer.cornerRadius = 8
    //    dataView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension UIView {
    func makeCircular() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.clipsToBounds = true
    }
}
