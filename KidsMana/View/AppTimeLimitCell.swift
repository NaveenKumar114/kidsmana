//
//  AppTimeLimitCell.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-19.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//

import UIKit
protocol AppTimeDelegate {
    func onClickTimer(index : IndexPath)
    func switchChanged(index : IndexPath , state : Bool)
}
class AppTimeLimitCell: UITableViewCell {

    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var appTimeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var leftSuper: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var mainBackground: UIView!
    
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    var indexPath : IndexPath!
    var delegate : AppTimeDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
      timeLabel.font = UIFont(name: "Digital-7 Italic", size: 19)!
        timeLabel.text = "00:00:00 Hr"
        backgroundColor = UIColor.clear
        
        self.shadowLayer.layer.borderWidth = 1
        self.shadowLayer.layer.cornerRadius = 3
        self.shadowLayer.layer.borderColor = UIColor.clear.cgColor
        self.shadowLayer.layer.masksToBounds = true

        self.layer.shadowOpacity = 0.18
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.masksToBounds = false
        
       
        
    }
 
    
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        delegate?.switchChanged(index: indexPath, state: sender.isOn)
    }
    @IBAction func timerButtonPressed(_ sender: Any) {
        delegate?.onClickTimer(index: indexPath)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
